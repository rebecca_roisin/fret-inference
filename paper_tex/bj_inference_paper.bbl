\begin{thebibliography}{34}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{ }

\bibitem[Ha et~al.({1996})Ha, Enderle, Ogletree, Chemla, Selvin, and
  Weiss]{ha96}
Ha, T., T.~Enderle, D.~F. Ogletree, D.~S. Chemla, P.~R. Selvin, and S.~Weiss,
  {1996}.
\newblock {Probing the interaction between two single molecules: Fluorescence
  resonance energy transfer between a single donor and a single acceptor}.
\newblock \emph{Proc. Natl. Acad. Sci. USA} {93}:{6264--6268}.

\bibitem[Haran({2003})]{haran03}
Haran, G., {2003}.
\newblock {Single-molecule fluorescence spectroscopy of biomolecular folding}.
\newblock \emph{{J. Phys.: Condens. Matter}} {15}:{R1291--R1317}.

\bibitem[Schuler et~al.({2002})Schuler, Lipman, and Eaton]{schuler02}
Schuler, B., E.~A. Lipman, and W.~A. Eaton, {2002}.
\newblock {Probing the free-energy surface for protein folding with
  single-molecule fluorescence spectroscopy}.
\newblock \emph{{Nature}} {419}:{743--747}.

\bibitem[Weiss({2000})]{weiss00}
Weiss, S., {2000}.
\newblock {Measuring conformational dynamics of biomolecules by single molecule
  fluorescence spectroscopy}.
\newblock \emph{{Nat. Struct. Mol. Biol}} {7}:{724--729}.

\bibitem[Deniz et~al.(2001)Deniz, Lawrence, Dahan, Chemla, Schultz, and
  Weiss]{deniz01}
Deniz, A.~A., T.~A. Lawrence, M.~Dahan, D.~S. Chemla, P.~S. Schultz, and
  S.~Weiss, 2001.
\newblock Ratiometric single-molecule studies of freely diffusing molecules.
\newblock \emph{Annu. Rev. Phys. Chem.} 52:233--253.

\bibitem[Gell et~al.(2006)Gell, Brockwell, and Smith]{gell06}
Gell, C., D.~Brockwell, and A.~Smith, 2006.
\newblock Handbook of single molecule fluorescence.
\newblock Oxford University Press, Oxford.

\bibitem[Ying et~al.(2000)Ying, Wallace, Balsubramanian, and Klenerman]{ying00}
Ying, L., M.~I. Wallace, S.~Balsubramanian, and D.~Klenerman, 2000.
\newblock Ratiometric analysis of single-molecule fluorescence resonance energy
  transfer using logical combinations of threshold criteria: a study of 12-mer
  DNA.
\newblock \emph{J. Phys. Chem. B.} 104:5171--5178.

\bibitem[Nir et~al.({2006})Nir, Michalet, Hamadani, Laurence, Neuhauser,
  Kovchegov, and Weiss]{nir06}
Nir, E., X.~Michalet, K.~M. Hamadani, T.~A. Laurence, D.~Neuhauser,
  Y.~Kovchegov, and S.~Weiss, {2006}.
\newblock {Shot-noise limited single-molecule FRET histograms: Comparison
  between theory and experiments}.
\newblock \emph{J. Phys. Chem. B} {110}:{22103--22124}.

\bibitem[Vogelsang et~al.(2007)Vogelsang, Doose, Sauer, and
  Tinnefeld]{vogelsang07}
Vogelsang, J., S.~Doose, M.~Sauer, and P.~Tinnefeld, 2007.
\newblock Single-molecule fluorescence resonance energy transfer in nanopipets:
  Improving distance resolution and concentration range.
\newblock \emph{Anal. Chem} 79:7367--7375.

\bibitem[Horrocks et~al.(2012)Horrocks, Li, Shim, Ranasinghe, Clarke, Huck,
  Abell, and Klenerman]{horrocks12}
Horrocks, M.~H., H.~Li, J.~Shim, R.~T. Ranasinghe, R.~W. Clarke, W.~T.~S. Huck,
  C.~Abell, and D.~Klenerman, 2012.
\newblock Single Molecule Fluorescence under Conditions of Fast Flow.
\newblock \emph{Anal. Chem.} 84:179--185.

\bibitem[Deniz et~al.(1999)Deniz, Laurence, Grunwell, Ha, Chemla, Weiss, and
  Schultz]{deniz99}
Deniz, A., T.~Laurence, J.~Grunwell, A.~Ha, T.J. an~Faulhaber, D.~Chemla,
  S.~Weiss, and P.~Schultz, 1999.
\newblock Single-pair fluorescence resonance energy transfer on freely
  diffusing molecules: observatrion of Förster distance dependence and
  subpopulations.
\newblock \emph{Proc. Natl. Acad. Sci. USA.} 96:3670--3675.

\bibitem[Kapanidis et~al.(2005)Kapanidis, Laurence, Lee, Margeat, Kong, and
  Weiss]{kapanidis05}
Kapanidis, A., T.~Laurence, N.~Lee, E.~Margeat, X.~Kong, and S.~Weiss, 2005.
\newblock Alternating-laser excitation of single molecules.
\newblock \emph{Acc. Chem. Res.} 38:532--533.

\bibitem[Muller et~al.({2005})Muller, Zaychikov, Brauchle, and Lamb]{muller05}
Muller, B.~K., E.~Zaychikov, C.~Brauchle, and D.~C. Lamb, {2005}.
\newblock {Pulsed interleaved excitation}.
\newblock \emph{{Biophys. J.}} {89}:{3508--3522}.

\bibitem[Doose et~al.(2006)Doose, Heilemann, Michalet, Weiss, and
  Kapanidis]{doose07}
Doose, S., M.~Heilemann, X.~Michalet, S.~Weiss, and A.~N. Kapanidis, 2006.
\newblock Periodic acceptor excitation spectroscopy of single molecules.
\newblock \emph{Eur. Biophys. J.} 36:669--674.

\bibitem[Eggeling et~al.(2001)Eggeling, Berger, Brand, Fries, Schaffer,
  Volkmer, and C.A.M.]{eggeling01}
Eggeling, C., S.~Berger, L.~Brand, J.~Fries, J.~Schaffer, A.~Volkmer, and
  S.~C.A.M., 2001.
\newblock Data registration and selective single-molecule analysis using
  multi-parameter fluorescence detection.
\newblock \emph{J. Biotechnol.} 86:163--180.

\bibitem[Santoso et~al.(2010)Santoso, Torella, and Kapanidis]{santoso10}
Santoso, Y., J.~P. Torella, and A.~N. Kapanidis, 2010.
\newblock Characterizing SIngle-Molecule FRET Dynamics with Probability
  Distribution Analysis.
\newblock \emph{ChemPhysChem} 11:2209--2219.

\bibitem[Barber(2012)]{barber12}
Barber, D., 2012.
\newblock Bayesian Reasoning and Machine Learning.
\newblock Cambridge University Press, Cambridge, UK.

\bibitem[Bayes and Price(1763)]{bayes63}
Bayes, T., and R.~Price, 1763.
\newblock An Essay towards solving a Problem in the Doctrine of Chance. By the
  late Rev. Mr. Bayes, communicated by Mr. Price, in a letter to John Canton,
  M. A. and F. R. S.
\newblock \emph{Phil. Trans. R. Soc. London} 53:370--418.

\bibitem[MacKay(2003)]{mackay03}
MacKay, D.~J., 2003.
\newblock Information Theory, Inference, and Learning Algorithms.
\newblock Cambridge University Press, Cambridge, UK.

\bibitem[McKinney et~al.(2006)McKinney, Joo, and Ha]{mckinney06}
McKinney, S.~A., C.~Joo, and T.~Ha, 2006.
\newblock Analysis of Single Molecule FRET Trajectories Using Hidden Markov
  Modeling.
\newblock \emph{Biophysical Journal} 91:1941--1951.

\bibitem[Bronson et~al.(2009)Bronson, Fei, Hofman, Gonzalez, and
  Wiggins]{bronson09}
Bronson, J.~E., J.~Fei, J.~M. Hofman, R.~N. Gonzalez, and C.~H. Wiggins, 2009.
\newblock Learning Rates and States from Biophysical Time Series: A Bayesian
  Approach to Model Selection and Single-Molecule FRET Data.
\newblock \emph{Biophys. J.} 97:3196--3205.

\bibitem[Bronson et~al.(2010)Bronson, Hofman, Fei, Gonzales, and
  Wiggins]{bronson10}
Bronson, J.~E., J.~M. Hofman, J.~Fei, R.~L. Gonzales, and C.~H. Wiggins, 2010.
\newblock Graphical models for inferring single molecule dynamics.
\newblock \emph{BMC Bioinformatics} 11:2--10.

\bibitem[Taylor et~al.(2010)Taylor, Makarov, and Landes]{taylor10}
Taylor, J.~N., D.~E. Makarov, and C.~F. Landes, 2010.
\newblock Denoising single-molecule FRET trajectories with wavelets and
  Bayesian inference.
\newblock \emph{Biophys. J.} 98:164--173.

\bibitem[Taylor and Landes(2011)]{taylor11}
Taylor, J.~N., and C.~F. Landes, 2011.
\newblock Improved resolution of complex single-molecule FRET systems via
  wavelet shrinkage.
\newblock \emph{J. Phys. Chem. B.} 115:1105--1114.

\bibitem[Yoon et~al.(2008)Yoon, Bruckbauer, Fitzgerald, and Klenerman]{yoon08}
Yoon, J.~W., A.~Bruckbauer, W.~J. Fitzgerald, and D.~Klenerman, 2008.
\newblock Bayesian Inference for Improved Single Molecule Fluorescence
  Tracking.
\newblock \emph{Biophys. J.} 94:4932--4947.

\bibitem[Turkcan et~al.(2012)Turkcan, Alexandrou, and Masson]{turkcan12}
Turkcan, S., A.~Alexandrou, and J.-B. Masson, 2012.
\newblock A Bayesian inference scheme to extract diffusivity and potenetial
  fields from confined single-molecule trajectories.
\newblock \emph{Biophys. J.} 102:2288--2298.

\bibitem[Kugel et~al.(2011)Kugel, Muschielok, and Michaelis]{kugel12}
Kugel, W., A.~Muschielok, and J.~Michaelis, 2011.
\newblock Bayesian-inference-based fluorescence correlation spectroscopy and
  single-molecule burst analysis reveal the influence of dye selection on DNA
  hairpin dynamics.
\newblock \emph{Chemphyschem} 13:1013--1022.

\bibitem[Guo et~al.(2011)Guo, He, Monnier, Sun, Wohland, and Bathe]{guo11}
Guo, S.-M., J.~He, N.~Monnier, G.~Sun, T.~Wohland, and M.~Bathe, 2011.
\newblock Bayesian approach to the analysis of fluorescence correlation
  spectroscopy data II: Application to simulated and in vitro data.
\newblock \emph{Anal. Chem.} 84:3880--3888.

\bibitem[He et~al.(2011)He, Guo, and Bathe]{he11}
He, J., S.-M. Guo, and M.~Bathe, 2011.
\newblock Bayesian approach to the analysis of fluorescence correlation
  spectroscopy data II: Theory.
\newblock \emph{Anal. Chem.} 84:3871--3879.

\bibitem[Kou et~al.({2005})Kou, Xie, and Liu]{kou05}
Kou, S.~C., X.~S. Xie, and J.~S. Liu, {2005}.
\newblock {Bayesian analysis of single-molecule experimental data}.
\newblock \emph{{J. R. Stat. Soc.}} {54}:{469--496}.

\bibitem[DeVore et~al.({2012})DeVore, Gull, and Johnson]{devore12}
DeVore, M.~S., S.~F. Gull, and C.~K. Johnson, {2012}.
\newblock {Classic Maximum Entropy Recovery of the Average Joint Distribution
  of Apparent FRET Efficiency and Fluorescence Photons for Single-Molecule
  Burst Measurements}.
\newblock \emph{{J. Phys. Chem. B.}} {116}:{4006--4015}.

\bibitem[Gopich and Szabo(2007)]{gopich07}
Gopich, I.~V., and A.~Szabo, 2007.
\newblock Single-Molecule FRET with Diffusion and Conformational Dynamics.
\newblock \emph{J. Phys. Chem. B.} 111:12925--12932.

\bibitem[Gopich and Szabo(2003)]{gopich03}
Gopich, I.~V., and A.~Szabo, 2003.
\newblock Single-Macromolecule Fluorescence Resonance Energy Transfer and
  Free-Energy Profiles.
\newblock \emph{J. Phys. Chem. B.} 107:5058--5063.

\bibitem[Schuler(2005)]{schuler05}
Schuler, B., 2005.
\newblock Single-molecule fluorescence spectroscopy of protein folding.
\newblock \emph{Chemphyschem} 6:1206--1220.

\end{thebibliography}
