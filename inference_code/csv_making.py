import csv
import scipy
import numpy as np
from config import Config 
##50_pop_p6_size100000_15.cfg

update = range(20)

f = file("/media/TOSHIBA EXT/inference_code/50_pop_p6_size100000_15.cfg")
cfg = Config(f)


for i in update:
    print i
    cfg.input.donor_name = "blue_50_pop_p6_single_size100000_15_%s.dat" %i
    g = file("test%s.cfg" %i, 'w')
    cfg.save(g)

f.close()
