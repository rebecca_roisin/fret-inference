# importing the libraries needed to run the program
import numpy as np
from scipy import stats
import math as ma
import random
import matplotlib.pyplot as plt
import cPickle

import ConfigParser
import sys
import cProfile

# Forward model of data to test fitting
def model_fwd_ps(state, lam_NB, lam_NR, p_labelB, p_labelR, p_config1, lam_DB, r_config1, r_config2, R_blue, gamma_ins, R0, count):
    # initialising lists of blue and red photons
    data_blue = []
    data_red = []
    proteins = [0.0, 0.0, 0.0]

    for i in range(count):
        # initialising the photon count for each channel
        blue = 0.0 
        red = 0.0
 
        # determining whether there is a protein present or not
        #state1 = stats.poisson.rvs(lam_prot1)
        #state2 = stats.poisson.rvs(lam_prot2)
        
        # regardless of protein state, there will be a contribution from the noise distribution
        blue += stats.poisson.rvs(lam_NB)
        red += stats.poisson.rvs(lam_NR)

        # how many proteins?
        x = range(2, state+1)
        x1 = random.choice(x)
        proteins[x1-2] += 1
        
        for j in range(x1):
            # conformational state of protein
            config1 = random.random() < p_config1

            # labelling state of protein
            labelB = random.random() < p_labelB
            labelR = random.random() < p_labelR

            if not labelB and not labelR:
                # if there are no labels, see noise only
                continue

            elif not labelB and labelR:
                # if there is only a red label, see noise only                
                continue

            elif labelB and not labelR:
                # if there is only a blue label, draw a lambda from a gamma distribution with mean lam_DB
                lam_blue = random.gammavariate(R_blue, lam_DB/R_blue)
                # the blue dye emits photons from a poisson distribution with a mean that is the sum of the noise and dye means
                blue += stats.poisson.rvs(lam_blue)

            elif labelB and labelR:
                if config1:
                # if both dyes are present, see FRET
                # equation for FRET efficiency
                    E1 = 1.0 / (1.0 + ((float(r_config1)/R0)**6))
                
                # draw sample lambda from gamma distribution and modify using FRET equation
                    lam_blue = random.gammavariate(R_blue, (lam_DB/R_blue))
                    lam_blue_post = lam_blue*(1 - E1)
                    lam_red = lam_blue*gamma_ins*E1
                else:
                    E2 = 1.0 / (1.0 + ((float(r_config2)/R0)**6))
                
                # draw sample lambda from gamma distribution and modify using FRET equation
                    lam_blue = random.gammavariate(R_blue, (lam_DB/R_blue))
                    lam_blue_post = lam_blue*(1 - E2)
                    lam_red = lam_blue*gamma_ins*E2
                    
                blue += stats.poisson.rvs(lam_blue_post)
                red += stats.poisson.rvs(lam_red)

        

        data_blue.append(int(blue))
        data_red.append(int(red))
    return data_blue, data_red, proteins

def vector_model(p_labelB, p_labelR, p_config1, lam_DB, r_config1, r_config2, R_blue, gamma_ins, R0, count):
    
    # number of noise, blue and both events
    none = (1 - p_labelB)*(1 - p_labelR) + (1 - p_labelB)*p_labelR
    blue = (1 - p_labelR)*p_labelB
    both = p_labelB*p_labelR

    distrib1 = np.random.multinomial(count, [none, blue, both*p_config1, both*(1-p_config1)])
    #print "distribution = ", distrib1
    num_none = distrib1[0]
    num_blue = distrib1[1]
    # num_both = distrib1[2] + distrib1[3]
    nc1 = distrib1[2] # int(np.random.binomial(num_both, p_config1))
    nc2 = distrib1[3] # num_both - nc1

    # noise vectors for total vector
    #noiseB = stats.poisson.rvs(lam_NB, size = count)
    #noiseR = stats.poisson.rvs(lam_NR, size = count)
    
    #both1 = both*p_config1*count
    #both2 = both*p_config2*count
    # noise only vectors
    blank_blue = np.zeros(num_none)
    blank_red = np.zeros(num_none + num_blue)
    #noiseB = stats.poisson.rvs(lam_NB, size = num_none)
    #noiseR = stats.poisson.rvs(lam_NR, size = (num_none+num_blue))
    # blue vector
    bluelam = stats.gamma.rvs(R_blue, loc = 0, scale = (lam_DB/R_blue), size = num_blue)
    blue = stats.poisson.rvs(bluelam)
    # both vectors
    bluelam1 = stats.gamma.rvs(R_blue, loc = 0, scale = (lam_DB/R_blue), size = nc1)
    bluelam2 = stats.gamma.rvs(R_blue, loc = 0, scale = (lam_DB/R_blue), size = nc2)
    E1 = 1.0 / (1.0 + ((float(r_config1)/R0)**6)) 
    E2 = 1.0 / (1.0 + ((float(r_config2)/R0)**6))
    lamblue1 = bluelam1*(1 - E1)
    redlam1 = bluelam1*gamma_ins*E1
    lamblue2 = bluelam2*(1 - E2)
    redlam2 = bluelam2*gamma_ins*E2
    blue_both1 = stats.poisson.rvs(lamblue1)
    blue_both2 = stats.poisson.rvs(lamblue2)
    red_both1 = stats.poisson.rvs(redlam1)
    red_both2 = stats.poisson.rvs(redlam2)
    blue_total = np.hstack((blank_blue, blue, blue_both1, blue_both2))
    red_total =  np.hstack((blank_red, red_both1, red_both2))
    random.shuffle(blue_total)
    random.shuffle(red_total)
    #blue_total = blue_total + noiseB
    #red_total = red_total + noiseR 
    #print "lengths = ", len(blue_total), len(red_total)
    return blue_total, red_total

def vector_general(freq, lam_NB, lam_NR, p_labelB, p_labelR, p_config1, lam_DB, r_config1, r_config2, R_blue, gamma_ins, R0, count):
    # Use linearity of means to compute the mean here
    none = (1 - p_labelB) # *(1 - p_labelR) + (1 - p_labelB)*p_labelR
    blue = p_labelB * (1 - p_labelR)
    both1 = p_labelB * p_labelR * p_config1
    both2 = p_labelB * p_labelR * (1 - p_config1)

    E1 = 1.0 / (1.0 + ((float(r_config1)/R0)**6)) 
    E2 = 1.0 / (1.0 + ((float(r_config2)/R0)**6))

    mean_blue = lam_NB + (blue * lam_DB + both1 * (1-E1) * lam_DB + both2 * (1-E2) * lam_DB) * freq
    mean_red = lam_NR + (both1 * E1 * gamma_ins * lam_DB + both2 * E2 * gamma_ins * lam_DB) * freq
    return mean_blue, mean_red

    # Previous sampling based mean estimation
    blue = []
    red = []
    for i in range(freq):
        b, r = vector_model(p_labelB, p_labelR, p_config1, lam_DB, r_config1, r_config2, R_blue, gamma_ins, R0, count)
        blue.append(b)
        red.append(r)
    blue = sum(blue)
    red = sum(red)
    noise_blue = stats.poisson.rvs(lam_NB, size = count)
    noise_red = stats.poisson.rvs(lam_NR, size = count)
    blue = blue + noise_blue
    red = red + noise_red
    bm = np.mean(blue)
    rm = np.mean(red)
    return bm, rm      
              

def model_fwd(state, lam_NB, lam_NR, p_labelB, p_labelR, p_config1, lam_DB, r_config1, r_config2, R_blue, gamma_ins, R0, count):
    # initialising lists of blue and red photons
    data_blue = []
    data_red = []

    for i in range(count):
        # initialising the photon count for each channel
        blue = 0.0 
        red = 0.0
 
        # determining whether there is a protein present or not
        #state1 = stats.poisson.rvs(lam_prot1)
        #state2 = stats.poisson.rvs(lam_prot2)
        
        # regardless of protein state, there will be a contribution from the noise distribution
        blue += stats.poisson.rvs(lam_NB)
        red += stats.poisson.rvs(lam_NR)

        # enumerating the options for a protein - assuming that there is maximum one protein present at any time
        
        for j in range(state):
            # conformational state of protein
            config1 = random.random() < p_config1

            # labelling state of protein
            labelB = random.random() < p_labelB
            labelR = random.random() < p_labelR

            if not labelB and not labelR:
                # if there are no labels, see noise only
                continue

            elif not labelB and labelR:
                # if there is only a red label, see noise only                
                continue

            elif labelB and not labelR:
                # if there is only a blue label, draw a lambda from a gamma distribution with mean lam_DB
                lam_blue = random.gammavariate(R_blue, lam_DB/R_blue)
                # the blue dye emits photons from a poisson distribution with a mean that is the sum of the noise and dye means
                blue += stats.poisson.rvs(lam_blue)

            elif labelB and labelR:
                if config1:
                # if both dyes are present, see FRET
                # equation for FRET efficiency
                    E1 = 1.0 / (1.0 + ((float(r_config1)/R0)**6))
                
                # draw sample lambda from gamma distribution and modify using FRET equation
                    lam_blue = random.gammavariate(R_blue, (lam_DB/R_blue))
                    lam_blue_post = lam_blue*(1 - E1)
                    lam_red = lam_blue*gamma_ins*E1
                else:
                    E2 = 1.0 / (1.0 + ((float(r_config2)/R0)**6))
                
                # draw sample lambda from gamma distribution and modify using FRET equation
                    lam_blue = random.gammavariate(R_blue, (lam_DB/R_blue))
                    lam_blue_post = lam_blue*(1 - E2)
                    lam_red = lam_blue*gamma_ins*E2
                    
                blue += stats.poisson.rvs(lam_blue_post)
                red += stats.poisson.rvs(lam_red)

        

        data_blue.append(int(blue))
        data_red.append(int(red))
    return data_blue, data_red

def model_clean(p_config1, lam_DB, r_config1, r_config2, R_blue, gamma_ins, R0, count):
    # initialising lists of blue and red photons
    data_blue = []
    data_red = []

    for i in range(count):
        # initialising the photon count for each channel
        blue = 0.0 
        red = 0.0
 
        # conformational state of protein
        config1 = random.random() < p_config1
        
        if config1:
        # if both dyes are present, see FRET
        # equation for FRET efficiency
            E1 = 1.0 / (1.0 + ((float(r_config1)/R0)**6))
        
        # draw sample lambda from gamma distribution and modify using FRET equation
            lam_blue = random.gammavariate(R_blue, (lam_DB/R_blue))
            lam_blue_post = lam_blue*(1 - E1)
            lam_red = lam_blue*gamma_ins*E1
        else:
            E2 = 1.0 / (1.0 + ((float(r_config2)/R0)**6))
        
        # draw sample lambda from gamma distribution and modify using FRET equation
            lam_blue = random.gammavariate(R_blue, (lam_DB/R_blue))
            lam_blue_post = lam_blue*(1 - E2)
            lam_red = lam_blue*gamma_ins*E2
            
            blue += stats.poisson.rvs(lam_blue_post)
            red += stats.poisson.rvs(lam_red)
        if blue != 0 and red != 0:
            data_blue.append(int(blue))
            data_red.append(int(red))
    data_blue = np.array(data_blue)
    data_red = np.array(data_red)        
    return data_blue, data_red

def getConfig(configname):

    params = {'R_blue': 1.0, 
                                        
                                        'lam_NB': 0.65,
                                        'lam_NR': 1.5,
                                        'p_labelB': 0.85,
                                        'p_labelR': 0.85,
                                        'lam_DB': 10.0,
                                        'R0': 56.0,
                                        'r_sep1': 70.0,
                                        'r_sep2': 40.0,
                                        'gamma_ins': 1.0,
                                        'count': 1000,
                                        'repeat': 10,
                                        "output_name": "Standard_Data_%s.dat"}

    config = ConfigParser.RawConfigParser(params)
    config.read(configname)

    for p in ['lam_NB', 'lam_NR', 'p_labelB', 'p_labelR', 'lam_DB', 'r_sep1', 'r_sep2', 'R0', 'gamma_ins']:
        params[p] = config.getfloat('FRET', p)
    for p in ["count", "repeat"]:
        params[p] = config.getint('Synthetic', p)
    params["output_name"] = config.get('Synthetic', "output_name")
    
    return params

if __name__=="__main__":
    params = getConfig(sys.argv[1])

    # Typical parameters
    R_blue = params["R_blue"]
    lam_NB = params["lam_NB"]
    lam_NR = params["lam_NR"]
    p_labelB = params["p_labelB"]
    p_labelR = params["p_labelR"]
    lam_DB = params["lam_DB"]
    r_sep1 = params["r_sep1"]
    r_sep2 = params["r_sep2"]
    R0 = params["R0"]
    gamma_ins = params["gamma_ins"]    
    p_config1 = 0.4   

    count = params["count"]
    repeat = params["repeat"]
    output_name = params["output_name"]
  
    E_expected1 = 1.0 / (1.0 + ((r_sep1/R0)**6))
    E_expected2 = 1.0 / (1.0 + ((r_sep2/R0)**6))
    
    print "expected efficiencies = ", E_expected1, E_expected2

    ## Vector method
    # generating noise distributions
    noise_blue = stats.poisson.rvs(lam_NB, size = count)
    noise_red = stats.poisson.rvs(lam_NR, size = count)

    # vector method - two proteins
    blue_total21, red_total21 = vector_model(p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)
    blue_total22, red_total22 = vector_model(p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)
    blue_total2 = blue_total21 + blue_total22 + noise_blue
    red_total2 = red_total21 + red_total22 + noise_red
    
    #print blue_total2, red_total2, len(blue_total2), len(red_total2)

    # vector method - three proteins
    blue_total31, red_total31 = vector_model(p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)
    blue_total32, red_total32 = vector_model(p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)
    blue_total33, red_total33 = vector_model(p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)
    blue_total3 = blue_total31 + blue_total32 + blue_total33 + noise_blue
    red_total3 = red_total31 + red_total32 + red_total33 + noise_red
  
    #print blue_total3, red_total3, len(blue_total3), len(red_total3)

    # vector method - four proteins
    blue_total41, red_total41 = vector_model(p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)
    blue_total42, red_total42 = vector_model(p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)
    blue_total43, red_total43 = vector_model(p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)
    blue_total44, red_total44 = vector_model(p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)
    blue_total4 = blue_total41 + blue_total42 + blue_total43 + blue_total44 + noise_blue
    red_total4 = red_total41 + red_total42 + red_total43 + red_total44 + noise_red

    
    # means from 2, 3, 4 proteins - vector method
    bm2 = np.mean(blue_total2)
    rm2 = np.mean(red_total2)

    bm3 = np.mean(blue_total3)
    rm3 = np.mean(red_total3)

    bm4 = np.mean(blue_total4)
    rm4 = np.mean(red_total4)
    
    print "Vector method means - blue", bm2, bm3, bm4
    print "Vector method means - red", rm2, rm3, rm4

    # means for two proteins
    blue2, red2 = model_fwd(2, lam_NB, lam_NR, p_labelB, p_labelR, p_config1, 
                          lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)

    bbm2 = np.mean(blue2)
    br2 = np.mean(red2)

    # means for three proteins
    blue3, red3 = model_fwd(3, lam_NB, lam_NR, p_labelB, p_labelR, p_config1,
                          lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)

    bbm3 = np.mean(blue3)
    br3 = np.mean(red3)

    # means for four proteins
    blue4, red4 = model_fwd(4, lam_NB, lam_NR, p_labelB, p_labelR, p_config1, 
                          lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)

    bbm4= np.mean(blue4)
    br4 = np.mean(red4)

    print "Blue means - non vector", bbm2, bbm3, bbm4
    print "Red means - non vector", br2, br3, br4

    blue_total, red_total, proteins_total = model_fwd_ps(4, lam_NB, lam_NR, p_labelB, p_labelR, p_config1, 
                          lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)

    btm = np.mean(blue_total)
    rtm = np.mean(red_total)
    print "Combined means", btm, rtm, proteins_total

    # generating combined means from individual means
    p2 = proteins_total[0] / count
    p3 = proteins_total[1] / count
    p4 = proteins_total[2] / count

    tmb = p2*bm2 + p3*bm3 + p4*bm4
    tmr = p2*rm2 + p3*rm3 + p4*rm4

    ttmb = p2*bbm2 + p3*bbm3 + p4*bbm4
    ttmr = p2*br2 + p3*br3 + p4*br4

    # generating means using vector function
    b2, r2 = vector_general(2, lam_NB, lam_NR, p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)
    b3, r3 = vector_general(3, lam_NB, lam_NR, p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)
    b4, r4 = vector_general(4, lam_NB, lam_NR, p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)

    totb = p2*b2 + p3*b3 + p4*b4
    totr = p2*r2 + p3*r3 + p4*r4

    print "Generated means (vector) = ", tmb, tmr   
    print "Generated means (non vector) = ", ttmb, ttmr
    print "Generated means (vector function) = ", totb, totr

    cProfile.run("vector_model(p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)")
    cProfile.run("model_fwd_ps(4, lam_NB, lam_NR, p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)") 
    cProfile.run("vector_general(4, lam_NB, lam_NR, p_labelB, p_labelR, p_config1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)")


