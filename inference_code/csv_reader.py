import csv
import scipy
import numpy as np

f_new = open('/media/TOSHIBA EXT/paper_figures/synthetic_data_population_size_3repeats.csv', 'w')
f_new.write("dataset, lam_mean, lam_min, lam_max, r_mean, r_min, r_max, E_mean, E_max, E_min\n")

def csv_parser(basefile, var1, var2):
    for v in var1:
        for w in var2:
            f = open('%s/stored_samples_%s_pop_p6_single_size100000r_%s.dat.csv' %(basefile, v, w), 'rb')
            reader = csv.reader(f, delimiter=',')
            lam_list = []
            r_list = []
            for row in reader:
                lam_list.append(row[0])
                r_list.append(row[2])
                #print row[0]
            lam_list.pop(0)
            r_list.pop(0)

            lam_list = sorted(lam_list)
            r_list = sorted(r_list)
            #print len(r_list)

            lam_list = lam_list[1:-1]
            r_list = r_list[1:-1]

            rr = []
            ll = []

            for l in lam_list:
                ll.append(float(l))

            for r in r_list:
                rr.append(float(r))

            rr = np.array(rr)
            ll = np.array(ll)

            E = 1.0 / (1.0 + (rr/56.0)**6)
            #print E 

            #print rr
            r_mean = np.mean(rr)

            print len(rr)

            #f_new.write("dataset, lam_mean, lam_min, lam_max, r_mean, r_min, r_max\n")
            f_new.write("%s, %s, %s, %s, %s, %s, %s, %s, %s, %s\n" %("%s_%s" %(v, w), np.mean(ll), ll[0], ll[-1], np.mean(rr), rr[0], rr[-1], np.mean(E), E[0], E[-1]))
            f.close()
#stored_samples_50_single_size1000_1.dat.csv
basefile = "/media/TOSHIBA EXT/paper_figures"
#file1 = "stored_samples_70_single_size100000_10.dat.csv"
#stored_samples_50_single_size100000_noise0p1_15.dat.csv
#stored_samples_40_pop_p1_single_size100000_0.dat.pkl
#stored_samples_50_single_size100000_ratep008_19.dat
#stored_samples_80_pop_p6_single_size100000r_1
#filelist = []
#rep = range(20)
rep = ["0", "1", "2"]
size = ["1000", "10000", "100000", "1000000"]
#r_sep = ["30", "35", "40", "50", "60", "70", "80", "85", "90"]
r_sep = ["40", "50", "60", "70", "80"]
sep = ["4bp", "6bp", "8bp", "10bp", "12bp"]
noise = ["0p1", "2", "4", "8"]
pop = ["p2", "p4", "p6", "p8", "p1"]
pops = ["p1", "p08", "p06", "p04", "p02", "p01", "p008"]
#csv_parser(basefile, size, rep)
#csv_parser(basefile, r_sep, rep)
#csv_parser(basefile, noise, rep)
#csv_parser(basefile, r_sep, pop)
csv_parser(basefile, r_sep, rep)
#csv_parser(basefile, r_sep, rep)
f_new.close()


