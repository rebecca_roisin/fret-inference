# importing the libraries needed to run the program
import ConfigParser
import os.path
import os
import FRET_forward as Ff
import PAX_sampler_nbinom_class as psnc

# What we want the program to do...
# Get a config file
# Use the config file to generate synthetic data, which is stored in a folder
# Access the synthetic data and analyse it
# Store the results of the analysis.

def test_data_generation(data_name, filepath):
    # make the synthetic data file from the config file
    try:
        print "Generating synthetic data..."
        data_path = "%s/%s.cfg" %(filepath, data_name)
        Ff.main(data_path)
        
    except:
        print "I tried to access a config file at %s.  This is not a valid filepath.  Please check that the filepath (%s) and the file (%s) exist." %(data_path, filepath, data_name)
        return
    print "Done!  Made synthetic data."

    # analyse the synthetic data using the inference config file 
    print "Starting analysis of the synthetic data" 
    
    try:  
        infer_cfg = "../test_data/cfg_data/%s/data0000_infer.cfg" %(data_name)
        psnc.main(infer_cfg)
    except:
        print "I tried to access a config file at %s.  This is not a valid filepath.  Please check that the filepath (%s) and the file (%s) exist." %(infer_cfg, 

data_name = "test"
filepath = "../test_data/cfg_data"
test_data_generation(data_name, filepath)
    
