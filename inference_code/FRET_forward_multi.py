# importing the libraries needed to run the program
import numpy as np
from scipy import stats
import math as ma
import random
import matplotlib.pyplot as plt
import cPickle

import ConfigParser
import sys
import os.path
import os

# Forward model of data to test fitting
def model_fwd(lam_prot1, lam_prot2, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r_config1, r_config2, R_blue, gamma_ins, R0, count):
    # initialising lists of blue and red photons
    data_blue = []
    data_red = []

    for i in range(count):
        # initialising the photon count for each channel
        blue = 0.0 
        red = 0.0
 
        # determining whether there is a protein present or not
        state1 = stats.poisson.rvs(lam_prot1)
        state2 = stats.poisson.rvs(lam_prot2)
        
        # regardless of protein state, there will be a contribution from the noise distribution
        blue += stats.poisson.rvs(lam_NB)
        red += stats.poisson.rvs(lam_NR)

        # enumerating the options for a protein - assuming that there is maximum one protein present at any time
        
        for j in range(state1):
            # conformational state of protein
            #config1 = random.random() < p_config1

            # labelling state of protein
            labelB = random.random() < p_labelB
            labelR = random.random() < p_labelR

            if not labelB and not labelR:
                # if there are no labels, see noise only
                continue

            elif not labelB and labelR:
                # if there is only a red label, see noise only                
                continue

            elif labelB and not labelR:
                # if there is only a red label, draw a lambda from a gamma distribution with mean lam_DB
                lam_blue = random.gammavariate(R_blue, lam_DB/R_blue)
                # the blue dye emits photons from a poisson distribution with a mean that is the sum of the noise and dye means
                blue += stats.poisson.rvs(lam_blue)

            elif labelB and labelR:
                # if both dyes are present, see FRET
                # equation for FRET efficiency
                E = 1.0 / (1.0 + ((float(r_config1)/R0)**6))
                
                # draw sample lambda from gamma distribution and modify using FRET equation
                lam_blue = random.gammavariate(R_blue, (lam_DB/R_blue))
                lam_blue_post = lam_blue*(1 - E)
                # draw red photons from a poisson with mean E*lam_NB, modified by "gamma factor" - an empirical constant to account for efficiency of red and blue photon counters 
                lam_red = lam_blue*gamma_ins*E # + lam_NR
                blue += stats.poisson.rvs(lam_blue_post)
                red += stats.poisson.rvs(lam_red)

        for j in range(state2):
            # conformational state of protein
            #config1 = random.random() < p_config1

            # labelling state of protein
            labelB = random.random() < p_labelB
            labelR = random.random() < p_labelR

            if not labelB and not labelR:
                # if there are no labels, see noise only
                continue

            elif not labelB and labelR:
                # if there is only a red label, see noise only                
                continue

            elif labelB and not labelR:
                # if there is only a red label, draw a lambda from a gamma distribution with mean lam_DB
                lam_blue = random.gammavariate(R_blue, lam_DB/R_blue)
                # the blue dye emits photons from a poisson distribution with a mean that is the sum of the noise and dye means
                blue += stats.poisson.rvs(lam_blue)

            elif labelB and labelR:
                # if both dyes are present, see FRET
                # equation for FRET efficiency
                E = 1.0 / (1.0 + ((float(r_config2)/R0)**6))
                
                # draw sample lambda from gamma distribution and modify using FRET equation
                lam_blue = random.gammavariate(R_blue, (lam_DB/R_blue))
                lam_blue_post = lam_blue*(1 - E)
                # draw red photons from a poisson with mean E*lam_NB, modified by "gamma factor" - an empirical constant to account for efficiency of red and blue photon counters 
                lam_red = lam_blue*gamma_ins*E # + lam_NR
                blue += stats.poisson.rvs(lam_blue_post)
                red += stats.poisson.rvs(lam_red)

        data_blue.append(int(blue))
        data_red.append(int(red))
    return data_blue, data_red

def getConfig(configname):

    params = {'R_blue': 1.0, 
                                        'lam_prot1': 0.02,
                                        'lam_prot2': 0.03,
                                        'lam_NB': 0.65,
                                        'lam_NR': 1.5,
                                        'p_labelB': 0.85,
                                        'p_labelR': 0.85,
                                        'lam_DB': 10.0,
                                        'R0': 56.0,
                                        'r_sep1': 70.0,
                                        'r_sep2': 40.0,
                                        'gamma_ins': 1.0,
                                        'count': 150000,
                                        'repeat': 10,
                                        "output_name": "Standard_Data_%s.dat"}

    config = ConfigParser.RawConfigParser(params)
    config.read(configname)

    for p in ["lam_prot1", "lam_prot2", 'lam_NB', 'lam_NR', 'p_labelB', 'p_labelR', 'lam_DB', 'r_sep1', 'r_sep2', 'R0', 'gamma_ins']:
        params[p] = config.getfloat('FRET', p)
    for p in ["count", "repeat"]:
        params[p] = config.getint('Synthetic', p)
    params["output_name"] = config.get('Synthetic', "output_name")

    # Define the output folders for parameters and data
    base_dir = os.path.dirname(configname)
    base_name = os.path.basename(configname).split(".")[0]

    # The base directory
    main_folder_name = os.path.join(base_dir, base_name)
    print "Store in: " + main_folder_name
    if not os.path.exists(main_folder_name):
        os.makedirs(main_folder_name)

    params["basedir"] = main_folder_name

    results_folder_name = os.path.join(main_folder_name, "results")
    if not os.path.exists(results_folder_name):
        os.makedirs(results_folder_name)

    params["resultsdir"] = results_folder_name
        
    return params

def main(config_name):
    params = getConfig(config_name)

    # Typical parameters
    R_blue = params["R_blue"]
    lam_prot1 = params["lam_prot1"]
    lam_prot2 = params["lam_prot2"]
    lam_NB = params["lam_NB"]
    lam_NR = params["lam_NR"]
    p_labelB = params["p_labelB"]
    p_labelR = params["p_labelR"]
    lam_DB = params["lam_DB"]
    r_sep1 = params["r_sep1"]
    r_sep2 = params["r_sep2"]
    R0 = params["R0"]
    gamma_ins = params["gamma_ins"]    
    
    count = params["count"]
    repeat = params["repeat"]
    output_name = params["output_name"]
    basedir = params["basedir"]

    # Write a parameter file
    params_string = "".join(["%s=%s\n" % (p, params[p]) for p in params])
    param_filename = os.path.join(basedir, "parameters.txt")

    f = file(output_name, "w")
    f.write(params_string)        
    f.close()
    
    print "Use parameters:"
    print params_string

    E_expected1 = 1.0 / (1.0 + ((r_sep1/R0)**6))
    E_expected2 = 1.0 / (1.0 + ((r_sep2/R0)**6))
    print "expected efficiencies = ", E_expected1, E_expected2

    for r in range(repeat):       
        blue, red = model_fwd(lam_prot1, lam_prot2, lam_NB, lam_NR, p_labelB, p_labelR, 
                              lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, count)
        # Write a pickled file
        blue_file = os.path.join(basedir, "data%04d_blue.dat" % r)
        red_file = os.path.join(basedir, "data%04d_red.dat" % r)
        
        f_blue = file(blue_file, "w")
        f_red = file(red_file, "w")

        cPickle.dump(blue, f_blue)
        cPickle.dump(red, f_red)

        f_blue.close()
        f_red.close()      
        
        # Now make an inference config file
        config = ConfigParser.RawConfigParser()
        config.add_section('input')
        config.set('input', 'filepath', basedir)
        config.set('input', 'donor_name', os.path.basename(blue_file))
        config.set('input', 'acceptor_name', os.path.basename(red_file))
        config.set('input', 'file_number', 1)

        config.add_section('variable parameters')
        config.set('variable parameters', 'p_labelB', p_labelB)
        config.set('variable parameters', 'p_labelR', p_labelR)

        blue_graph = "data%04d_marginalB.pdf" % r
        red_graph = "data%04d_marginalR.pdf" % r
        samples =  "data%04d_samples.dat" % r
        outfolder = params["resultsdir"]            

        config.add_section('output')
        config.set('output', 'blue_graph', blue_graph)
        config.set('output', 'red_graph', red_graph)
        config.set('output', 'sample_file', samples)
        config.set('output', 'output_filepath', outfolder)
        
        settings = [("burn_in_1", 3000), ("iterations_1", 1000),
("samples_1", 2), \

                    ("burn_in_2", 1000),
("iterations_2",100),
("samples_2", 100)]

        config.add_section('iterator parameters')
        for k,v in settings:
            config.set('iterator parameters', k, v)

        infer_file = os.path.join(basedir, "data%04d_infer.cfg" % r)
        with open(infer_file, 'wb') as configfile:
            config.write(configfile)

if __name__=="__main__":
    cfg_name = sys.argv[1]
    main(cfg_name)
