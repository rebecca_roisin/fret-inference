from scipy import stats
import numpy as np
np.seterr(invalid='raise')

import math as ma
import random
import numpy.random as npr
import cPickle
import array
from scipy.special import gamma, gammaln
import MCS_reader
import matplotlib.pyplot as plt
import profile
from metropolis_multi import *
from FRET_standard import *
import sys
import FRET_forward_pseudomulti as ffp
import os.path
import copy
import ConfigParser

from integrate import *

# Making a Metropolis-Hastings Monte-Carlo sampler that will estimate the parameters of Poisson distributions that fit 2-colour FRET data.  Initially, we will assume a single population.  This will be extended to multiple FRET populations once the simple sampler runs.

#Collecting (synthetic) data from a file
def file_reader(file1):
    f = open(file1, "r")
    data = cPickle.load(f)
    data = np.array(data)
    print data
    f.close()
    return data

# Function that finds the maximum of the posterior distribution of lambdas.
def quad_solve(E, lam_NB, blue, r, lam_DB):
    x = 1.0 - E
    a = -((x**2) + (r*x/lam_DB)) 
    b = (blue*x) + (x*r) - (x + (x*lam_NB) + (r*lam_NB/lam_DB))
    c = (lam_NB*r) - lam_NB

    g = (b**2 - (4*a*c))**0.5
    x1 = (-b + g) / (2*a)
    x2 = (-b - g) / (2*a)
    return x2


# Function that takes two data inputs and creates a dictionary of the frequencies at which different pairs of values (from the two inputs) occur.
def data_counter(data_blue, data_red):
    # print data_blue, data_red
    blue_array = []
    red_array = []
    counter = []
    counted_data = {}
    for i, j in zip(data_blue, data_red):
        if (i,j) not in counted_data:
            counted_data[(i,j)] = 0
        counted_data[(i,j)] += 1
    for (i,j), k in counted_data.iteritems():
        blue_array.append(i)
        red_array.append(j)
        counter.append(k)
    blue_array = np.array(blue_array)
    red_array = np.array(red_array)
    counter = np.array(counter)
    print "length of input = %s \nlength of output = %s \n" %(len(data_blue), len(blue_array))
    return blue_array, red_array, counter
   
# preparing the cache for the poisson_broad function
seq_ref = np.array(range(300))
gamma_ref = gamma(seq_ref + 1)
dist_cache = {}
cache_len = 0

# Function that samples from a Poisson distribution    
def poisson_pmf(data, lam):
    global dist_cache, cache_len
    if lam in dist_cache:
        # Cache maintenance
        ppmf = dist_cache[lam]        
    else:
        exp = ma.e**(-lam)
        ppmf = ((lam**seq_ref) / gamma(seq_ref + 1))*exp 
        # x = ((lam**data) / gamma(data + 1))*exp
        if cache_len > 1000:
            dist_cache = {}
            cache_len = 0
        #binom[binom < 0.00000000000001] = 0.0
        dist_cache[lam] = ppmf
        cache_len += 1
    
    return ppmf[data]

# Function that samples from an overdispersed poisson distribution
def poisson_broad(data, lam, r = 4):
    global dist_cache, cache_len
    if (r, lam) in dist_cache:
        # Cache maintenance
        binom = dist_cache[(r, lam)]        
    else:
        p = lam / (r + lam)
        coeff = ma.e**(gammaln(seq_ref + r) - ((gammaln(seq_ref + 1) + gammaln(r))))
        binom = coeff * ((1-p)**r) * (p**seq_ref)
        if cache_len > 1000:
            dist_cache = {}
            cache_len = 0
        #binom[binom < 0.00000000000001] = 0.0
        dist_cache[(r, lam)] = binom
        cache_len += 1
    
    return binom[data]

def poisson_broad_slow(data, lam, r = 4):

    p = lam / (r + lam)
    #coeff = gamma(data + r) / ((gamma(data + 1) * gamma(r)))
    coeff = ma.e**(gammaln(data+r) - (gammaln(data+1) + gammaln(r)))
    binom = coeff * ((1-p)**r) * (p**data)
    return binom


#np.seterr(over = "raise")

def poisson_broad_sum2(data, lam1, R1, lam2, R2):
    global dist_cache, cache_len
    if (R1, lam1, R2, lam2) in dist_cache:
        ps = dist_cache[(R1, lam1, R2, lam2)]
    else:
        p1 = poisson_broad(seq_ref, lam1, R1)
        p2 = poisson_broad(seq_ref, lam2, R2)
        ps = np.convolve(p1, p2, "full")
        #if ma.isinf(sum(ps)):
        #    print ps
        #    print p1
        #    print p2
        #    raise Exception("PS infinite!")
        if cache_len > 1000:
            dist_cache = {}
            cache_len = 0
        dist_cache[(R1, lam1, R2, lam2)] = ps
        cache_len += 1
    return ps[data]

def ML_partition(data, lam1, R1, lam2, R2):
    p1 = poisson_broad(seq_ref, lam1, R1)
    p2 = poisson_broad(seq_ref, lam2, R2)
    v = np.zeros(300)
    for i in range(300):
        v[i] = np.argmax(p2[np.arange(i + 1)] * p1[np.arange(i, -1, -1)])
    newlam = (v + R2) / ((R2 / lam2) + 1) # Mean of posterior gamma
    return newlam[data]

# function to deal with four possibilities for protein
def protein_fwd(lam_NB, lam_BR, pLabelB, pLabelR, lam_DB, r, R_blue, gamma_ins, R0):

    labelB = random.random() < pLabelB
    labelR = random.random() < pLabelR
    if not labelB and not labelR:
        blue = stats.poisson.rvs(lam_NB)
        red = stats.poisson.rvs(lam_NR)
        
    elif not labelB and labelR:
        blue = stats.poisson.rvs(lam_NB)
        red = stats.poisson.rvs(lam_NR)
        
    elif labelB and not labelR:
        lam_blue = random.gammavariate(R_blue, lam_DB/R_blue)
        blue = stats.poisson.rvs(lam_NB + lam_blue)
        # blue += int(lam_blue)
    elif labelB and labelR:
        # r_sep = stats.norm.rvs(r, 2.0)
        E = 1.0 / (1.0 + ((float(r)/R0)**6))
        lam_blue = random.gammavariate(R_blue, (lam_DB/R_blue))
        lam_blue_post = lam_blue*(1 - E) + lam_NB
        lam_red = lam_blue*gamma_ins*E + lam_NR
        blue = stats.poisson.rvs(lam_blue_post)
        red = stats.poisson.rvs(lam_red)
    return blue, red

# Forward model of data to test fitting
def model_fwd(lam_prot1, lam_prot2, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r_1, r_2, R_blue, gamma_ins, R0, count, total_points):
    hist_blue = np.zeros(300)
    hist_red = np.zeros(300)
    data = []

    norm_poisson1 = poisson_pmf(np.array(range(4)), lam_prot1)
    norm_poisson2 = poisson_pmf(np.array(range(4)), lam_prot2)
    #bias_poisson = poisson_pmf(np.array(range(4)), 1.0) 

    for i in range(count):
        w1 = 1.0
        w2 = 1.0
        w_path = 1.0

        blue = 0.0 #stats.poisson.rvs(lam_NB)
        red = 0.0 #stats.poisson.rvs(lam_NR)
        # blue = int(lam_NB)
        # red = int(lam_NR)    
        
        # selection = stats.poisson.rvs(1.0)
        selection = random.choice([0,1])#,2,3])
        # print "selection = ", selection
        w1 *= norm_poisson1[selection]        
        w2 *= norm_poisson2[selection]        
        # w_path *= bias_poisson[selection]
        w_path *= 0.5

        blue = stats.poisson.rvs(lam_NB)
        red = stats.poisson.rvs(lam_NR)

        for prot in range(min(selection,1)):
            blue1, red1 = protein_fwd(lam_NB, lam_BR, pLabelB, pLabelR, lam_DB, r_1, R_blue, gamma_ins, R0)
            blue2, red2 = protein_fwd(lam_NB, lam_BR, pLabelB, pLabelR, lam_DB, r_2, R_blue, gamma_ins, R0)
            
        if (blue1 < 300) and (red1 < 300):
            hist_blue[blue] += w1 / w_path
            hist_red[red] += w1 / w_path

        if (blue2 < 300) and (red2 < 300):
            hist_blue[blue] += w2 / w_path
            hist_red[red] += w2 / w_path

    hist_blue = total_points * hist_blue / sum(hist_blue)
    hist_red = total_points * hist_red / sum(hist_red)
    return hist_blue, hist_red

def distribution(data_blue, data_red, counter, value):
    red_x_array = np.zeros(300)
    # blue, red, counter = data
    for b, r, count in zip(data_blue, data_red, counter):
        if b == value:
            red_x_array[r] += count
    print red_x_array
    return red_x_array    

# test function?
def random_func():
    table = np.zeros((125, 125))
    variable = poisson_pmf(np.arange(125), 9)
    for i in range(125):
        for j in range(125):
            table[i,j] = variable[max(i, j)]
    return table

# table = random_func()
# print "table = ", table

def protein_probability_model(data_blue, data_red, counter, p_config, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r_sep, R_blue, gamma_ins, R0, E, p_blue, p_both, prints, ApproxModel, R_approx = 1.5):

    r_noise = 50

    if ApproxModel:
        ## Approximation using best lambda
        pB_FRET_1 =  poisson_broad_sum2(data_blue, lam_NB, r_noise, (lam_DB*(1.0-E)), R_blue)    
        blue_prime = quad_solve(E, lam_NB, data_blue, R_blue, lam_DB)
        pR_FRET_1 = pB_FRET_1 * poisson_broad_slow(data_red, lam_NR + (gamma_ins*blue_prime*E), R_approx)
       
    else:
        pR_FRET_1 = cachedmanymc(data_blue, data_red, lam_NB, lam_NR, R_blue, E, gamma_ins, lam_DB, prints, samples = 200)
    
    return pR_FRET_1
                            
                        
# Core probability evaluator
def model(data_blue, data_red, counter, params, prints, ApproxModel = True):

    # lam_prot1, lam_prot2, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0
    lam_prot1, lam_prot2, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, R_approx = params.lam_prot1, params.lam_prot2, params.lam_NB, params.lam_NR, params.pLabelB, params.pLabelR, params.lam_DB, params.r_sep1, params.r_sep2, params.R_blue, params.gamma_ins, params.R0, params.R_approx 

    ## Some wide priors for the data
    prior = 1.0 #* stats.norm.pdf(lam_prot1, 0.03, 5.0)
    #prior *= stats.norm.pdf(lam_prot2, 0.03, 5.0)

    if prior < 1.0 / (10**6):
        print "Low prior", lam_prot1, lam_prot2

    ## Likelihood function

    lam_total = lam_prot1 + lam_prot2

    p_prot0, p_prot1, p_prot2, p_prot3, p_prot4 = poisson_pmf(0, lam_total), poisson_pmf(1, lam_total), poisson_pmf(2, lam_total), poisson_pmf(3, lam_total), poisson_pmf(4, lam_total)

    p_prot_many = 1.0 - (p_prot0 + p_prot1)

    p_config1 = p_prot1 * (lam_prot1 / lam_total)
    p_config2 = p_prot1 * (lam_prot2 / lam_total)

    p2m = p_prot2 / (p_prot2 + p_prot3 + p_prot4)
    p3m = p_prot3 / (p_prot2 + p_prot3 + p_prot4)
    p4m = p_prot4 / (p_prot2 + p_prot3 + p_prot4)

    # Blue channel probabilities 
    E1 = 1.0 / (1 + ((float(r_sep1) / R0)**6))
    E2 = 1.0 / (1 + ((float(r_sep2) / R0)**6))

    # New probabilities using different labeling probabilities
    x = p_labelB
    y = p_labelR

    p_none = (1-x)*(1-y)
    p_blue = x*(1-y)
    p_red = (1-x)*y
    p_both = x*y        

    r_noise = 50  

    # probability from noise only
    P_noise = p_prot0 + (p_prot1 * (p_none + p_red))
    pB_noise = P_noise * poisson_pmf(data_blue, lam_NB) # , r_noise)
    pR_noise = pB_noise * poisson_pmf(data_red, lam_NR)

    # probability from one blue dye
    P_dye_1 = p_prot1 * p_blue
    pB_dye_1 = P_dye_1 * poisson_broad_sum2(data_blue, lam_NB, r_noise, lam_DB, R_blue)
    pR_dye_1 = pB_dye_1 * poisson_pmf(data_red, lam_NR)

    # probability from protein in configuration 1
    P_FRET_1_c1 = p_config1 * p_both
    p_tot_R1 = P_FRET_1_c1 * protein_probability_model(data_blue, data_red, counter, p_config1, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r_sep1, R_blue, gamma_ins, R0, E1, p_blue, p_both, prints, ApproxModel, R_approx = 1.5)

    # probability from protein in configuration 2
    P_FRET_1_c2 = p_config2 * p_both
    p_tot_R2 = P_FRET_1_c2 * protein_probability_model(data_blue, data_red, counter, p_config2, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r_sep2, R_blue, gamma_ins, R0, E2, p_blue, p_both, prints, ApproxModel, R_approx = 1.5)

    # new case - many proteins including different configurations.  Approximated by sampling many times to find mean for 2, 3, 4, proteins:
    pc1 = lam_prot1 / (lam_prot1 + lam_prot2)

    blue2, red2 = ffp.vector_general(2, lam_NB, lam_NR, p_labelB, p_labelR, pc1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, 100)
    blue3, red3 = ffp.vector_general(3, lam_NB, lam_NR, p_labelB, p_labelR, pc1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, 100)
    blue4, red4 = ffp.vector_general(4, lam_NB, lam_NR, p_labelB, p_labelR, pc1, lam_DB, r_sep1, r_sep2, R_blue, gamma_ins, R0, 100)
    
    multi_mean_blue = p2m*blue2 + p3m*blue3 + p4m*blue4
    multi_mean_red = p2m*red2 + p3m*red3 + p4m*red4 

    #print "multi means: blue = %s, red = %s \n" %(multi_mean_blue, multi_mean_red)
    #f.write("multi means: blue = %s, red = %s \n" %(multi_mean_blue, multi_mean_red))

    pB_many = p_prot_many * poisson_broad(data_blue, multi_mean_blue, 1.0)
    pR_many = pB_many * poisson_broad(data_red, multi_mean_red, 1.0)   
    
    # total probabilities
    p_total = pR_noise + pR_dye_1 + p_tot_R1 + p_tot_R2 + pR_many
    p_tot = np.log(p_total)
    p_tots = np.sum(p_tot * counter) + np.log(prior)

    try:
        assert(p_tots <= 0.0)
    except:
        #if p_tots > 0.0:
        print "Probability greater than One!"
        print "Total probability = %s, protein_1 = %s, protein_2 = %s, many = %s, noise = %s" %(p_tots, np.sum(np.log(p_tot_R1*counter)), np.sum(np.log(p_tot_R2*counter)), np.sum(np.log(pR_many*counter)), np.sum(np.log(pR_noise*counter)))

        flog = file("log.txt","w")
        for i in range(len(p_total)):
            flog.write("%s %s %s %s %s %s %s\n" % (data_blue[i], data_red[i], pR_noise[i], pR_dye_1[i], p_tot_R1[i], p_tot_R2[i], pR_many[i]))
        flog.close()

        raise    
    #f.close()
    if r_sep1 > r_sep2:
        print "r1 too big!"
        p_tots = -np.inf
    return p_tots, p_total, None
    

# Function that evaluates the probability that a dataset was generated from a given set of parameters (pProt, lam_NB, lam_NR, pBlue, pRed, lam_DB, lam_DR, r).  Evalulates individually each datapoint in the dataset.  Data should be in array form.  Complete but not tested.  
def probability_evaluator_fast(data, params, target, choice, prints=False):

    data_blue, data_red, counter = data
    params = Var_to_params(params, target, choice)

    logprob, _, _ = model(data_blue, data_red, counter, params, prints, ApproxModel = True)
    return logprob

## The evaluator that uses the slow function
def probability_evaluator(data, params, target, choice, prints=False):

    data_blue, data_red, counter = data
    params = Var_to_params(params, target, choice)

    logprob, _, _ = model(data_blue, data_red, counter, params, prints, ApproxModel = False)
    return logprob

class Model_params:
    def __init__(self):
        self.R_blue = 1.0
        self.lam_prot1 = 0.05
        self.lam_prot2 = 0.05
        self.lam_NB = 0.5
        self.lam_NR = 1.0
        self.pLabelB = 0.40
        self.pLabelR = 0.80
        self.lam_DB = 8.0
        self.r_sep1 = 40.0
        self.r_sep2 = 60.0   
        self.R0 = 56.0
        self.gamma_ins = 1.0
        self.R_approx = 20.0

    def __str__(self):
        return "Current values: lam_prot1 = %s, lam_prot2 = %s, lam_NB = %s, lam_NR = %s, plabelB = %s, pLabelR = %s, lam_DB = %s, r_sep1 = %s, r_sep2 = %s, R_blue = %s" %(self.lam_prot1, self.lam_prot2, self.lam_NB, self.lam_NR, self.pLabelB, self.pLabelR, self.lam_DB, self.r_sep1, self.r_sep2, self.R_blue)



# Function that initialises variables for the iterator
def initialise(params, ApproxModel = True):
    # gamma_ins, lam_prot1, lam_prot2, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r_sep1, r_sep2, R_blue, R0
    target = {}

    gamma_ins = Variable(params.gamma_ins, 0.1, logone_transform, "gamma", target)
    lam_prot_1 = Variable(params.lam_prot1, 0.02, logone_transform, "1st protein lambda", target)
    lam_prot_2 = Variable(params.lam_prot2, 0.02, logone_transform, "2nd protein lambda", target)
    lamNB = Variable(params.lam_NB, 0.5, logone_transform, "lambda noise blue", target)
    lamNR = Variable(params.lam_NR, 0.5, logone_transform, "lambda noise red", target)
    pLabelB = Variable(params.pLabelB, 0.2, clamp_transform(0.2, 1.0), "probability of blue label", target)
    pLabelR = Variable(params.pLabelR, 0.2, clamp_transform(0.1, 1.0), "probability of red label", target)  
    lamDB = Variable(params.lam_DB, 0.5, logone_transform, "lambda dye blue", target)
    
    r1 = Variable(params.r_sep1, 1.0, logone_transform, "r_sep1", target)
    r2 = Variable(params.r_sep2, 1.0, logone_transform, "r_sep2", target)    
    R_blue = Variable(params.R_blue, 0.5, logone_transform, "r_blue", target)
    R_approx = Variable(20, 0.5, logone_transform, "R_approx", target)
    
    switch_variables = [r1, r2]
    coswitch_variables = [lam_prot_1, lam_prot_2]

    if ApproxModel == True:
        sampling_variables = [lam_prot_1, lam_prot_2, lamNB, lamNR, lamDB, r1, r2]        
    else:
        sampling_variables = [lam_prot_1, lam_prot_2, lamDB, r1, r2]
        
    return sampling_variables, target, switch_variables, coswitch_variables              
    
def Var_to_params(params, target, choice):

    # gamma_ins, lam_prot1, lam_prot2, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r_sep1, r_sep2, R_blue, R0
    params = copy.copy(params)
    params.lam_prot1 = target["1st protein lambda"][choice]
    params.lam_prot2 = target["2nd protein lambda"][choice]
    params.lam_NB = target["lambda noise blue"][choice]
    params.lam_NR = target["lambda noise red"][choice]
    params.pLabelB = target["probability of blue label"][choice]
    params.pLabelR = target["probability of red label"][choice]
    params.lam_DB = target["lambda dye blue"][choice]
    params.r_sep1 = target["r_sep1"][choice]
    params.r_sep2 = target["r_sep2"][choice]
    params.R_blue = target["r_blue"][choice]
    params.gamma_ins = target["gamma"][choice]
    params.R_approx = target["R_approx"][choice]

    return params
    

def lambda_histogram(params, total_points, buckets = 50):

    data_blue = []
    data_red = []
    counter = []
    
    for b in range(buckets):
        for r in range(buckets):
            data_blue += [b]
            data_red += [r]            
            counter += [1]

    data_blue = np.array(data_blue)
    data_red = np.array(data_red)    
    counter  = np.array(counter)

    # model(data_blue, data_red, counter, lam_prot, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r, R_blue, gamma_ins, R0)

    _, probs, _ = model(data_blue, data_red, counter, params, prints=False, ApproxModel = False)

    print "Sum of probs == 1? %s" % (sum(probs))

    freq = probs * total_points
                
    blue_freq = np.zeros(buckets)
    red_freq = np.zeros(buckets)

    for (b, r, v) in zip(data_blue, data_red, freq):
         blue_freq[b] += v
         red_freq[r] += v

    return blue_freq, red_freq

def main_iterators(blue_data, red_data, counter, params,config):
    sample_storer = lambda p, t: Var_to_params(p, t, True)
    
    ## First phase -- get close to the variables
    sampling_variable, target, switch_names, coswitch_variables = initialise(params, ApproxModel = True)
    burn_in_1 = config.getint('iterator parameters', 'burn_in_1')
    iterations_1 = config.getint('iterator parameters', 'iterations_1')
    samples_1 = config.getint('iterator parameters', 'samples_1')

    (params.lam_prot1, params.lam_prot2, params.lam_NB, params.lam_NR, params.lam_DB, params.r_sep1, params.r_sep2), _ = iterator((blue_data, red_data, counter), sampling_variable, target, switch_names, coswitch_variables, params, probability_evaluator_fast, sample_storer, sample_number = samples_1, burn_in = burn_in_1, sample_iterations = iterations_1)

    print "\n\nPHASE 2 -- Precise estimation\n\n"
    print params

    burn_in_2 = config.getint('iterator parameters', 'burn_in_2')
    iterations_2 = config.getint('iterator parameters', 'iterations_2')
    samples_2 = config.getint('iterator parameters', 'samples_2')

    ## Second phase -- nail the exact values for r_sep
    sampling_variable, target, switch_names, coswitch_variables = initialise(params, ApproxModel = False)

    (params.lam_prot1, params.lam_prot2, params.lam_DB, params.r_sep1, params.r_sep2), samples = iterator((blue_data, red_data, counter), sampling_variable, target, switch_names, coswitch_variables, params, probability_evaluator, sample_storer, sample_number = samples_2, burn_in = burn_in_2, sample_iterations = iterations_2)
    
    return params, samples 

def main(configname):
    #bp = sys.argv[1]
    try:
        config = ConfigParser.RawConfigParser({})
        config.read(configname)

        filepath, blue_file, red_file, no_files = config.get('input', 'filepath'), config.get('input', 'donor_name'), config.get('input', 'acceptor_name'), config.getint('input', 'file_number')# , file_start, file_end, config.getint('input', 'file_start'), config.getint('input', 'file_end')
        #blue_file, red_file, no_files = sys.argv[1], sys.argv[2], int(sys.argv[3])
        print "File names read" 
    except:
        print "Usage: python PAX_sampler_nbinom_class_multi.py blue_pattern red_pattern number"
        print "/media/KINGSTON/20121026/3/DNA_d_ /media/KINGSTON/20121026/3/DNA_a_ 60"
        raise
    #print "Analysis for %s" % bp
    print "Analysis of DNA mixture data"

    data_blue1 = []
    data_red1 = []

    blue_channel = file_reader("%s/%s" %(filepath, blue_file))
    red_channel = file_reader("%s/%s" %(filepath, red_file))
    print "Here is the raw blue data:", blue_channel
    print blue_channel
    #try:
    #    for i in range(no_files):
    #        data_blue1 += MCS_reader.MCS_process("%s/%s%04d.mcs" % (filepath, blue_file, i) )
    #        data_red1 += MCS_reader.MCS_process("%s/%s%04d.mcs" % (filepath, red_file, i) )
    #except:
    #    print "Files may not exist"# , i        
    #    raise    

    
    ## Filter large values
    #data_blue = [db for (db,dr) in zip(data_blue1, data_red1) if db < 300 and dr < 300] 
    #data_red = [dr for (db,dr) in zip(data_blue1, data_red1) if db < 300 and dr < 300]

    data_blue = [db for (db,dr) in zip(blue_channel, red_channel) if db < 300 and dr < 300] 
    data_red = [dr for (db,dr) in zip(blue_channel, red_channel) if db < 300 and dr < 300]


    "Here is the top-filtered blue data:", data_blue
     
    blue_channel = np.array(data_blue)
    red_channel = np.array(data_red) 

    blue_data, red_data, counter = data_counter(blue_channel, red_channel)

    my_params = Model_params()
    my_params.pLabelB = config.getfloat('variable parameters', 'p_labelB')
    my_params.pLabelR = config.getfloat('variable parameters', 'p_labelR')
    
    print "\n\nPHASE 1 -- Approximate estimation\n\n"
    my_params, samples = main_iterators(blue_data, red_data, counter, my_params, config)
    print "Storing Samples"
    output_filepath = config.get('output', 'output_filepath')
    storage_name = config.get('output', 'sample_file')
    # pickled data
    #f_pickle = open("/media/TOSHIBA EXT/20121213/6_and_10_2_stored_samples.pkl", "w")
    f_pickle = open("%s/%s.pkl" %(output_filepath, storage_name), "w")    
    cPickle.dump(samples, f_pickle)
    f_pickle.close()

    # csv file
    #f_csv = open("/media/TOSHIBA EXT/20121213/6_and_10_2_stored_samples.csv", "w")
    f_csv = open("%s/%s.csv" %(output_filepath, storage_name), "w")
    f_csv.write("lam_prot1, lam_prot2, lamNB, lamNR, pLabelB, pLabelR, lamDB, r_sep1, r_sep2, R_blue, gamma_ins \n")
    for p in samples:
        f_csv.write("%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s \n" %(p.lam_prot1, p.lam_prot2, p.lam_NB, p.lam_NR, p.pLabelB, p.pLabelR, p.lam_DB, p.r_sep1, p.r_sep2, p.R_blue, p.gamma_ins))
        print p
    f_csv.close()
        

    print "\n\nPHASE 3 -- Plotting histograms\n\n"

    blue_freq, red_freq = lambda_histogram(my_params, sum(counter), buckets = 200)

    #1blue_freq, red_freq = lambda_histogram(lam_prot1, lam_prot2, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r_sep1, r_sep2, R0, sum(counter), R_blue, gamma_ins, 20.0, 50)

    #1hist_blue, hist_red = model_fwd(lam_prot1, lam_prot2, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r_1, r_2, R_blue, gamma_ins, R0, 50000, sum(counter))

    #1hist_blue = hist_blue[0:50]
    #1hist_red = hist_red[0:50]

    # file names for graphs
    blue_graph = config.get('output', 'blue_graph')
    red_graph = config.get('output', 'red_graph')

    blue_histogram = plt.hist(blue_channel, bins = range(200), log = True, facecolor='none')
    # plt.ylim(ymax=000)
    blue_lambda_plot = plt.plot(range(200), np.maximum(blue_freq, 10**-1), "bo")
    #1blue_hist_plot = plt.plot(range(50), np.maximum(hist_blue, 10**-1), "ro")
    plt.xlabel("Donor Photons", labelpad=6)
    plt.ylabel("Frequency", labelpad=6)
    #plt.savefig(r"/home/rebecca/Documents/Klenermanproject/Figures/data_blue_7.pdf")
    plt.savefig("%s/%s" %(output_filepath, blue_graph))
    #plt.show()
    plt.cla()

    red_histogram = plt.hist(red_channel, bins = range(200), log = True, facecolor='none')
    # plt.ylim(ymax=1000)
    red_lambda_plot = plt.plot(range(200), np.maximum(red_freq, 10**-1), "ro")
    #1red_hist_plot = plt.plot(range(50), np.maximum(hist_red, 10**-1), "ro")
    plt.xlabel("Acceptor Photons", labelpad=6)
    plt.ylabel("Frequency", labelpad=6)
    #1plt.savefig(r"/home/rebecca/Documents/Klenermanproject/Figures/data_red_7.pdf")
    plt.savefig("%s/%s" %(output_filepath, red_graph))
    #plt.show()
    #1plt.cla()

if __name__=="__main__":
    cfg_name = sys.argv[1]
    main(cfg_name)
