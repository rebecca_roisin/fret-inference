# importing the libraries needed to run the program
import ConfigParser
import os.path
import os
import FRET_forward as Ff
import PAX_sampler_nbinom_class as psnc
import csv_reader as csv_reader
import sys
import ConfigParser

# What we want the program to do...
# 0. get config file from terminal using sys.argv[1]
# 1. get config file -- done!
#    create synthetic data and analysis config file for each dataset -- done!
# 2. run a loop in which analysis is perormed for each dataset -- done!
# 3. get stored samples after each analysis, order the sample values and add the average, min and max to a stock file
# 4.generate a graph from the outputs?

#1. generating synthetic data
# make the synthetic data file from the config file

def data_generation(config_file):
    try:
        print "Generating synthetic data..."
        #data_path = "%s/%s.cfg" %(filepath, data_name)
        params = Ff.main(config_file, test = False)
        repeat = params["repeat"]
        basedir = params["basedir"]
        
    except:
        print "I tried to access a config file at %s.  This is not a valid filepath.  Please check that the filepath (%s) and the file (%s) exist." %(data_path, filepath, data_name)
        return
    print "Done!  Made synthetic data."
    print "But wait!  The number of repeats is: ", repeat
    print "What is the basedir?", params["basedir"]
    return params, basedir, repeat

def data_analysis(basedir, repeat):
    for i in range(repeat):
        try:  
            infer_cfg = "%s/data%04d_infer.cfg" %(basedir, i)
            print "I am about to analyse", infer_cfg
            psnc.main(infer_cfg, synthetic=True)
        except:
            print "I tried to access a config file at %s.  This is not a valid filepath.  Please check that the filepath (%s) and the file (%s) exist." %(infer_cfg, basedir, data_name) 
    #data%04d_infer.cfg

config_file = sys.argv[1]
data_name = ""
data_files = []
#filepath = "../test_data/cfg_data/50_synthetic_ratep06_100000"
#results_name = "outputs"
repeat = 1
params, basedir, repeat = data_generation(config_file)
for i in range(repeat):
    data_files.append("%s%04d_samples.dat.csv" %(data_name, i))
#print data_files
filepath, cfg_name = os.path.split(config_file)
print "No. repeats =", [x for x in range(repeat)]
data_analysis(basedir, repeat)
#print "data_name = ", data_name
#csv_reader.main(filepath, [data_name], repeat, filepath, results_name)
