# importing the libraries needed to run the program
import numpy as np
from scipy import stats
import math as ma
import random
import matplotlib.pyplot as plt
import sys
import cPickle
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import MCS_reader
# from PAX_simple import FRET_efficiency

def file_reader(filepath):
    f = open(filepath, "r")
    obj = cPickle.load(f)
    f.close()
    return obj

def data_process(datafile, datalist):
    #data = []    
    f = open(datafile, "r")
    datalist += MCS_reader.MCS_process(datafile)
    #data = np.array(data)
    f.close()
    return datalist

def FRET_efficiency(data_blue, F_FRET, R0, gamma = 1):
    data_blue = np.array(data_blue)
    F_FRET = np.array(F_FRET)
    data_blue = gamma*data_blue
    E = F_FRET / (data_blue + F_FRET)
    Ed = (1.0-E)/E
    separation = R0*(Ed**(1.0/6.0))
    return E, separation

def Zratio(blue, red):
    Z_list = []
    for i, j in zip(blue, red):
        Z = np.log(float(j)/i)
        Z_list.append(Z)
    return Z_list

#print "blue_data:", blue_data

def and_thresholder(blue, red, threshold_blue, threshold_red):
    blue_thresholded = []
    red_thresholded = []
    for b, r in zip(blue, red):
        if b > threshold_blue and r > threshold_red:
            blue_thresholded.append(float(b))
            red_thresholded.append(float(r))
    return blue_thresholded, red_thresholded

def sum_thresholder(blue, red, threshold):
    blue_thresholded = []
    red_thresholded = []
    for b, r in zip(blue, red):
        if (b + r) > threshold:
            blue_thresholded.append(float(b))
            red_thresholded.append(float(r))
    return blue_thresholded, red_thresholded

if __name__=="__main__":

    R0 = 56.0
    blue_threshold = 10
    red_threshold = 10
    E_means = []

    #basefile = "/media/TOSHIBA EXT/synthetic_data"
    basefile = "/media/TOSHIBA EXT/20130608/Ubq2_K6_NC"
    #ratios = ["80", "70", "60", "50", "40"]
    #populations = ["p2", "p4", "p6", "p8", "p1"]
    file_list = ["PBS_T20", "PBS_T20_USP21i"]
    rep = ["FRET"]
    #file_list = ["0M", "1M", "2M", "3M", "4M", "5M", "6M"]
    #filelist = ["8and12", "10and12"]#, "6and10"]
    #file_list = ["synthetic_40and60_1_88.dat", "synthetic_40and60_2_88.dat", "synthetic_40and60_3_88.dat", "synthetic_40and50_1_88.dat", "synthetic_40and50_2_88.dat", "synthetic_40and50_3_88.dat", "synthetic_40and70_1_88.dat", "synthetic_40and70_2_88.dat", "synthetic_40and70_3_88.dat", "synthetic_40and80_1_88.dat", "synthetic_40and80_2_88.dat", "synthetic_40and80_3_88.dat", "synthetic_50and60_1_88.dat", "synthetic_50and60_2_88.dat", "synthetic_50and60_3_88.dat", "synthetic_50and70_1_88.dat", "synthetic_50and70_2_88.dat", "synthetic_50and70_3_88.dat", "synthetic_50and80_1_88.dat", "synthetic_50and80_2_88.dat", "synthetic_50and80_3_88.dat", "synthetic_60and70_1_88.dat", "synthetic_60and70_2_88.dat", "synthetic_60and70_3_88.dat", "synthetic_60and80_1_88.dat", "synthetic_60and80_2_88.dat", "synthetic_60and80_3_88.dat", "synthetic_70and80_1_88.dat", "synthetic_70and80_2_88.dat", "synthetic_70and80_3_88.dat"]
    #file_list = ["synthetic_70and80_3_88.dat"]
    #blue_80_pop_p6_single_size100000r_2.dat
    files = ["30", "35", "85", "90"]
    
    #file_list = ["Standard_Data_single_40_100000_%s.dat", "Standard_Data_single_50_100000_%s.dat", "Standard_Data_single_60_100000_%s.dat", "Standard_Data_single_70_100000_%s.dat", "Standard_Data_single_80_100000_%s.dat"]
    #output_file = "/media/TOSHIBA EXT/synthetic_data"
    output_file = "/media/TOSHIBA EXT/20130608/Ubq2_K6_NC"
    # blue_synthetic_40and50_1_%s.dat
    #blue_30_synthetic_p6_size100000_0

    for r in rep:
        for f in file_list:
            #blue_file = "%s/blue_%s_synthetic_p6_size100000_%s.dat" %(basefile, f, r)
            #red_file = "%s/red_%s_synthetic_p6_size100000_%s.dat" %(basefile, f, r)

            don_blue = []
            don_red = []

            #don_blue = file_reader(blue_file)
            #don_red = file_reader(red_file)

            for k in range(120):
                don_blue = data_process("%s/%s/%s/UBQ_d_%04d.mcs" %(basefile, f, r, k), don_blue) 
                don_red = data_process("%s/%s/%s/UBQ_a_%04d.mcs" %(basefile, f, r, k), don_red) 

        #don_blue = file_reader("%s/blue_%s" %(basefile, f))
        #don_red = file_reader("%s/red_%s" %(basefile, f))
        
        # sum analysis
            blue_thresholded_sum, red_thresholded_sum = sum_thresholder(don_blue, don_red, 2*blue_threshold)
            E_sum, _ = FRET_efficiency(blue_thresholded_sum, red_thresholded_sum, R0, gamma = 1.0)

            # plotting E
            bins = np.arange(0.0, 1.0, 0.02)
            freq, binss, _ = plt.hist(E_sum, bins, facecolor = "grey")
            bin_centres = []
            for i in range(len(binss)-1):
                bin_centres.append((binss[i+1] + binss[i])/2)
            plt.xlabel("FRET Efficiency")
            plt.ylabel("Number of Events")
            plt.savefig("%s/E_sum_ppltn_10_%s_%s.png" %(output_file, f, r))
            plt.savefig("%s/E_sum_ppltn_10_%s_%s.pdf" %(output_file, f, r))
            fs_csv = open("%s/E_sum_ppltn_10_%s_%s.csv" %(output_file, f, r), "w")
            fs_csv.write("%s, %s \n" %("E", "Frequency"))
            for bc, fr in zip(bin_centres, freq):
                fs_csv.write("%s, %s \n" %(bc, fr))
            fs_csv.close()
            plt.show()
            plt.cla()

            # and analysis
            blue_thresholded_and, red_thresholded_and = and_thresholder(don_blue, don_red, blue_threshold, red_threshold)
            E_and, _ = FRET_efficiency(blue_thresholded_and, red_thresholded_and, R0, gamma = 1.0)

            # plotting E
            bins = np.arange(0.0, 1.0, 0.02)
            freq, binss, _ = plt.hist(E_and, bins, facecolor = "grey")
            bin_centres = []
            for i in range(len(binss)-1):
                bin_centres.append((binss[i+1] + binss[i])/2)
            plt.xlabel("FRET Efficiency")
            plt.ylabel("Number of Events")
            plt.savefig("%s/E_and_ppltn_10_%s_%s.png" %(output_file, f, r))
            plt.savefig("%s/E_and_ppltn_10_%s_%s.pdf" %(output_file, f, r))
            fa_csv = open("%s/E_and_ppltn_10_%s_%s.csv" %(output_file, f, r), "w")
            fa_csv.write("%s, %s \n" %("E", "Frequency"))
            for bc, fr in zip(bin_centres, freq):
                fa_csv.write("%s, %s \n" %(bc, fr))
            fa_csv.close()
            plt.show()
            plt.cla()


     



