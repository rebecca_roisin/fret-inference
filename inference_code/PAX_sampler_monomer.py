from scipy import stats
import numpy as np
np.seterr(invalid='raise')

import math as ma
import random
import numpy.random as npr
import cPickle
import array
from scipy.special import gamma, gammaln
import MCS_reader
import matplotlib.pyplot as plt
import profile
from metropolis_multi import *
from FRET_standard import *
import sys
import os.path
import copy
import ConfigParser

from integrate import *
from csv_tab import *
from binary_reader import parse_bin

# Making a Metropolis-Hastings Monte-Carlo sampler that will estimate the parameters of Poisson distributions that fit distributions of aggregating monomer data.  Initially, we will assume a single population.  This will be extended to multiple FRET populations once the simple sampler runs.

#Collecting (synthetic) data from a file
def file_reader(file1):
    f = open(file1, "r")
    data = cPickle.load(f)
    data = np.array(data)
    print data
    f.close()
    return data

# Function that finds the maximum of the posterior distribution of lambdas.
def quad_solve(E, lam_NB, blue, r, lam_DB):
    x = 1.0 - E
    a = -((x**2) + (r*x/lam_DB)) 
    b = (blue*x) + (x*r) - (x + (x*lam_NB) + (r*lam_NB/lam_DB))
    c = (lam_NB*r) - lam_NB

    g = (b**2 - (4*a*c))**0.5
    x1 = (-b + g) / (2*a)
    x2 = (-b - g) / (2*a)
    return x2


# Function that takes two data inputs and creates a dictionary of the frequencies at which different pairs of values (from the two inputs) occur.
def data_counter(data_blue, data_red):
    # print data_blue, data_red
    blue_array = []
    red_array = []
    counter = []
    counted_data = {}
    for i, j in zip(data_blue, data_red):
        if (i,j) not in counted_data:
            counted_data[(i,j)] = 0
        counted_data[(i,j)] += 1
    for (i,j), k in counted_data.iteritems():
        blue_array.append(i)
        red_array.append(j)
        counter.append(k)
    blue_array = np.array(blue_array)
    red_array = np.array(red_array)
    counter = np.array(counter)
    print "length of input = %s \nlength of output = %s \n" %(len(data_blue), len(blue_array))
    return blue_array, red_array, counter

def data_counter_blue(data_blue):
    blue_array = []
    counter = []
    counted_data = {}
    for i in data_blue:
        if i not in counted_data:
            counted_data[i] = 0
        counted_data[i] += 1
    for (i), k in counted_data.iteritems():
        blue_array.append(i)
        counter.append(k)
    blue_array = np.array(blue_array)
    counter = np.array(counter)
    print "length of input = %s \nlength of output = %s \n" %(len(data_blue), len(blue_array))
    return blue_array, counter

   
# preparing the cache for the poisson_broad function
seq_ref = np.array(range(300))
gamma_ref = gamma(seq_ref + 1)
dist_cache = {}
cache_len = 0

# Function that samples from a Poisson distribution    
def poisson_pmf(data, lam):
    global dist_cache, cache_len
    if lam in dist_cache:
        # Cache maintenance
        ppmf = dist_cache[lam]        
    else:
        lamLN = ma.log(lam)
        ppmf = ma.e**(seq_ref*lamLN - gammaln(seq_ref + 1) - lam)
        
        if cache_len > 1000:
            dist_cache = {}
            cache_len = 0
        #binom[binom < 0.00000000000001] = 0.0
        dist_cache[lam] = ppmf
        cache_len += 1
    
    return ppmf[data]

# Function that samples from an overdispersed poisson distribution
def poisson_broad(data, lam, r = 4):
    global dist_cache, cache_len
    if (r, lam) in dist_cache:
        # Cache maintenance
        binom = dist_cache[(r, lam)]        
    else:
        p = lam / (r + lam)
        coeff = ma.e**(gammaln(seq_ref + r) - ((gammaln(seq_ref + 1) + gammaln(r))))
        binom = coeff * ((1-p)**r) * (p**seq_ref)
        if cache_len > 1000:
            dist_cache = {}
            cache_len = 0
        #binom[binom < 0.00000000000001] = 0.0
        dist_cache[(r, lam)] = binom
        cache_len += 1
    
    return binom[data]

def poisson_broad_slow(data, lam, r = 4):

    p = lam / (r + lam)
    #coeff = gamma(data + r) / ((gamma(data + 1) * gamma(r)))
    coeff = ma.e**(gammaln(data+r) - (gammaln(data+1) + gammaln(r)))
    binom = coeff * ((1-p)**r) * (p**data)
    return binom


#np.seterr(over = "raise")

def poisson_broad_sum2(data, lam1, R1, lam2, R2):
    global dist_cache, cache_len
    if (R1, lam1, R2, lam2) in dist_cache:
        ps = dist_cache[(R1, lam1, R2, lam2)]
    else:
        p1 = poisson_broad(seq_ref, lam1, R1)
        p2 = poisson_broad(seq_ref, lam2, R2)
        ps = np.convolve(p1, p2, "full")

        if cache_len > 1000:
            dist_cache = {}
            cache_len = 0
        dist_cache[(R1, lam1, R2, lam2)] = ps
        cache_len += 1
    return ps[data]


def distribution(data_blue, data_red, counter, value):
    red_x_array = np.zeros(300)
    # blue, red, counter = data
    for b, r, count in zip(data_blue, data_red, counter):
        if b == value:
            red_x_array[r] += count
    print red_x_array
    return red_x_array    

                       
# Core probability evaluator
def model(data_blue, counter, params, ApproxModel = True):

    lam_prot, lam_NB, lamDB, R_blue, gamma_ins, R_approx = params.lam_prot, params.lam_NB, params.lamDB, params.R_blue, params.gamma_ins,  params.R_approx 

    # wide priors for the data
    prior = scipy.stats.norm.pdf(lamDB, 10, 5) * scipy.stats.expon.pdf(lam_prot, mean=0.01)

    p_prot0, p_prot1, p_prot2, p_prot3, p_prot4, p_prot5, p_prot6, p_prot7, p_prot8 = poisson_pmf(0, lam_prot), poisson_pmf(1, lam_prot), poisson_pmf(2, lam_prot), poisson_pmf(3, lam_prot), poisson_pmf(4, lam_prot) , poisson_pmf(5, lam_prot), poisson_pmf(6, lam_prot), poisson_pmf(7, lam_prot), poisson_pmf(8, lam_prot) 
   
    p_prot_many = 1.0 - p_prot0 - p_prot1
    
    r_noise = 50  

    # probability from noise only
    P_noise = p_prot0
    pB_noise = P_noise * poisson_pmf(data_blue, lam_NB)
        
    # probability from one blue dye (monomer)    
    P_dye_1 = p_prot1
    pB_dye_1 = P_dye_1 * poisson_broad_sum2(data_blue, lam_NB, r_noise, lamDB, R_blue) 

    # probability from two blue dyes (dimer)    
    P_dye_2 = p_prot2
    pB_dye_2 = P_dye_2 * poisson_broad_sum2(data_blue, 2*lam_NB, r_noise, lamDB, R_blue)      

    # probability from three blue dyes (trimer)    
    P_dye_3 = p_prot3
    pB_dye_3 = P_dye_3 * poisson_broad_sum2(data_blue, 3*lam_NB, r_noise, lamDB, R_blue)        
    
    # probability from four blue dyes (tetramer)    
    P_dye_4 = p_prot4
    pB_dye_4 = P_dye_4 * poisson_broad_sum2(data_blue, 4*lam_NB, r_noise, lamDB, R_blue)

    # probability from four blue dyes (tetramer)    
    P_dye_5 = p_prot5
    pB_dye_5 = P_dye_5 * poisson_broad_sum2(data_blue, 5*lam_NB, r_noise, lamDB, R_blue)         

    # probability from four blue dyes (tetramer)    
    P_dye_6 = p_prot6
    pB_dye_6 = P_dye_6 * poisson_broad_sum2(data_blue, 6*lam_NB, r_noise, lamDB, R_blue)      

    # probability from four blue dyes (tetramer)    
    P_dye_7 = p_prot7
    pB_dye_7 = P_dye_7 * poisson_broad_sum2(data_blue, 7*lam_NB, r_noise, lamDB, R_blue)      

    # probability from four blue dyes (tetramer)    
    P_dye_8 = p_prot8
    pB_dye_8 = P_dye_8 * poisson_broad_sum2(data_blue, 8*lam_NB, r_noise, lamDB, R_blue)        
    

    ## Sanity Check
    try:
        assert(0.99 < P_noise + P_dye_1 + P_dye_2 + P_dye_3 + P_dye_4 + P_dye_5 + P_dye_6 + P_dye_7 + P_dye_8 <= 1.01)
    except:
        raise

    p_tot_B = pB_noise + pB_dye_1 + pB_dye_2 + pB_dye_3 + pB_dye_4 + pB_dye_5 + pB_dye_6 + pB_dye_7 + pB_dye_8
    
    p_tot = np.log(p_tot_B)
    p_tots = np.sum(p_tot * counter) + np.log(prior)

    try:
        p_tots <= 0.0
    except:
        #if p_tots > 0.0:
        print "Probability greater than One!"
        print "Total probability = %s, protein_1 = %s, many = %s, noise = %s" %(p_tots, np.sum(np.log(p_tot_B*counter)), np.sum(np.log(pR_many*counter)), np.sum(np.log(pB_noise*counter)))
    
        print "Total: %s" % p_tot_B
        print "noise: %s" % sum(pB_noise)
        print "monomer: %s" % sum(pB_dye_1)
        print "dimer: %s" % sum(pB_dye_2)
        print "trimer: %s" % sum(pB_dye_3)
        for b,x in zip(data_blue, pB_dye_1):
            print "(%s,%s) %s" % (b,r,x)        
        raise
    
    if ma.isnan(p_tots) or ma.isinf(p_tots):
        print "noise: %s" % sum(pB_noise)
        print "monomer: %s" % sum(pB_dye_1)
        print "dimer: %s" % sum(pB_dye_2)
        print "trimer: %s" % sum(pB_dye_3)
        raise Exception("Nan or Inf: %s" % p_tots)

    # Return detailed counts, or not, depending on whether you are evaluating for histogram
    return p_tots, p_tot_B, None # prFRET
    

# Function that evaluates the probability that a dataset was generated from a given set of parameters (pProt, lam_NB, lam_NR, pBlue, pRed, lam_DB, lam_DR, r).  Evalulates individually each datapoint in the dataset.  Data should be in array form.  Complete but not tested.  
def probability_evaluator_fast(data, params, target, choice):

    data_blue, counter = data
    params = Var_to_params(params, target, choice)

    logprob, _, _ = model(data_blue, counter, params, ApproxModel = True)
    return logprob

## The evaluator that uses the slow function
def probability_evaluator(data, params, target, choice):

    data_blue, counter = data
    params = Var_to_params(params, target, choice)

    logprob, _, _ = model(data_blue, counter, params, ApproxModel = False)
    return logprob

class Model_params:
    def __init__(self):
        self.R_blue = 1.0
        self.lam_prot = 0.05
        #self.lam_prot2 = 0.05
        self.lam_NB = 0.5
        self.lamDB = 8.0
        self.gamma_ins = 1.0
        self.R_approx = 30.0

    def __str__(self):
        return "Current values: lam_prot = %s, lam_NB = %s, lam_DB = %s, R_blue = %s" %(self.lam_prot, self.lam_NB, self.lamDB, self.R_blue)

# Function that initialises variables for the iterator
def initialise(params, ApproxModel = True):

    target = {}

    gamma_ins = Variable(params.gamma_ins, 0.1, logone_transform, "gamma", target)
    lam_prot = Variable(params.lam_prot, 0.2, logone_transform, "protein lambda", target)
    lam_NB = Variable(params.lam_NB, 0.5, logone_transform, "lambda noise blue", target)
    
    lamDB = Variable(params.lamDB, 0.5, logone_transform, "lambda dye blue", target)  

    R_blue = Variable(params.R_blue, 0.5, logone_transform, "r_blue", target)
    R_approx = Variable(20, 0.5, logone_transform, "R_approx", target)
    
    if ApproxModel == True:
        sampling_variables = [lam_prot, lam_NB, lamDB]
    else:
        sampling_variables = [lam_prot, lam_NB, lamDB]

    return sampling_variables, target

def Var_to_params(params, target, choice):

    # gamma_ins, lam_prot1, lam_prot2, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r_sep1, r_sep2, R_blue, R0
    params = copy.copy(params)
    params.lam_prot = target["protein lambda"][choice]
    #params.lam_prot2 = target["2nd protein lambda"][choice]
    params.lam_NB = target["lambda noise blue"][choice]
    params.lamDB = target["lambda dye blue"][choice]
    params.R_blue = target["r_blue"][choice]
    params.gamma_ins = target["gamma"][choice]
    params.R_approx = target["R_approx"][choice]

    return params              
    

def lambda_histogram(params, total_points, buckets = 50):

    data_blue = []
    counter = []
    
    for b in range(buckets):
        data_blue += [b]
                    
        counter += [1]

    data_blue = np.array(data_blue)
    
    counter  = np.array(counter)

    # model(data_blue, data_red, counter, lam_prot, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r, R_blue, gamma_ins, R0)

    _, probs, _ = model(data_blue, counter, params)

    print "Sum of probs == 1? %s" % (sum(probs))

    freq = probs * total_points
                
    blue_freq = np.zeros(buckets)
   

    for (b, v) in zip(data_blue, freq):
         blue_freq[b] += v
         

    return blue_freq

def main_iterators(blue_data, counter, params, config):
    sample_storer = lambda p, t: Var_to_params(p, t, True)
    
    sampling_variable, target = initialise(params, ApproxModel = True)
    burn_in_1 = config.getint('iterator parameters', 'burn_in_1')
    iterations_1 = config.getint('iterator parameters', 'iterations_1')
    samples_1 = config.getint('iterator parameters', 'samples_1')

    (params.lam_prot, params.lam_NB, params.lamDB), _ = iterator((blue_data, counter), sampling_variable, target, None, None, params, probability_evaluator_fast, sample_storer, sample_number = samples_1, burn_in = burn_in_1, sample_iterations = iterations_1)

    print "\n\nPHASE 2 -- Precise estimation\n\n"    

    burn_in_2 = config.getint('iterator parameters', 'burn_in_2')
    iterations_2 = config.getint('iterator parameters', 'iterations_2')
    samples_2 = config.getint('iterator parameters', 'samples_2')

    ## Second phase -- nail the exact values for r_sep
    sampling_variable, target = initialise(params, ApproxModel = False)

    (params.lam_prot, params.lam_NB, params.lamDB), samples = iterator((blue_data, counter), sampling_variable, target, None, None, params, probability_evaluator, sample_storer, sample_number = samples_2, burn_in = burn_in_2, sample_iterations = iterations_2)
    
    return params, samples 

def main(configname, synthetic = False):
    #bp = sys.argv[1]
    #print "Analysis for %s" % bp
    if synthetic == True:
        try:
            config = ConfigParser.RawConfigParser({})
            config.read(configname)

            filepath, blue_file, no_files = config.get('input', 'filepath'), config.get('input', 'donor_name'), config.getint('input', 'file_number')
            #file_start, file_end = config.getint('input', 'file_start'), config.getint('input', 'file_end')
            #blue_file, red_file, no_files = sys.argv[1], sys.argv[2], int(sys.argv[3])
            print "File names read" 
        except:
            print "Usage: python PAX_sampler_nbinom_class_multi.py blue_pattern red_pattern number"
            print "/media/KINGSTON/20121026/3/DNA_d_ /media/KINGSTON/20121026/3/DNA_a_ 60"
            raise
        #print "Analysis for %s" % bp
        print "Analysis of DNA mixture data"

        blue_channel = file_reader("%s/%s" %(filepath, blue_file))
        print "Here is the raw blue data:", blue_channel
        data_blue = [db for db in blue_channel if db < 100]

    elif synthetic == False:
        config = ConfigParser.RawConfigParser({})
        config.read(configname)

        filepath, filename, no_files = config.get('input', 'filepath'), config.get('input', 'file_name'),  config.getint('input', 'file_number')
        file_start, file_end = config.getint('input', 'file_start'), config.getint('input', 'file_end')

        data_tuples = []        
        data_blue1 = []
        data_red1 = []

        try:
            for i in range(file_start, file_end):
                name = "%s%04d.dat" %(filename, i)
                data_tuples = parse_bin(filepath, name, data_tuples)
            for j in data_tuples:
                data_blue1.append(j[0])
                data_red1.append(j[1])
            #data_blue1, data_red1 = csv_tab("/media/TOSHIBA EXT/repos/fret-inference/test_data/monomers", "asyn0000.txt")
            print len(data_blue1)
                
        except:
            print "Files may not exist"# , i        
            raise    


    
    
        ## Filter large values
        #data_blue = [db for (db,dr) in zip(data_blue1, data_red1) if db < 100 and dr < 100] 
        #data_red = [dr for (db,dr) in zip(data_blue1, data_red1) if db < 100 and dr < 100]
        data_blue = [db for db in data_blue1 if db < 200]
        
        
    
    blue_data, counter = data_counter_blue(data_blue)
    #distribution(blue_data, red_data, counter, 10)

       
    # initialise(gamma_initial, lam_prot_initial, lamNB_initial, lamNR_initial, pLabelB_initial, pLabelR_initial, lamDB_initial, r_initial, R_blue_initial):
    my_params = Model_params()
    
    print "\n\nPHASE 1 -- Approximate estimation\n\n"
    my_params, samples = main_iterators(blue_data, counter, my_params, config)
    print "Storing Samples"    
    output_filepath = config.get('output', 'output_filepath')
    storage_name = config.get('output', 'sample_file')
    # pickled data
    #f_pickle = open("/media/TOSHIBA EXT/20121213/6_and_10_2_stored_samples.pkl", "w")
    f_pickle = open("%s/%s.pkl" %(output_filepath, storage_name), "w")    
    cPickle.dump(samples, f_pickle)
    f_pickle.close()

    # csv file
    #f_csv = open("/media/TOSHIBA EXT/20121213/6_and_10_2_stored_samples.csv", "w")
    f_csv = open("%s/%s.csv" %(output_filepath, storage_name), "w")
    f_csv.write("lam_NB, lam_prot, lamDB\n")
    for p in samples:
        f_csv.write("%s, %s, %s \n" %(p.lam_NB, p.lam_prot, p.lamDB))   
        # params.lam_prot, params.lam_NB, params.lamDB     
    f_csv.close()
        

    print "\n\nPHASE 3 -- Plotting histograms\n\n"

    blue_freq = lambda_histogram(my_params, sum(counter), buckets = 150)

    # file names for graphs
    blue_graph = config.get('output', 'blue_graph')
    
    blue_histogram = plt.hist(data_blue, bins = range(150), log = True, facecolor='none')
    # plt.ylim(ymax=000)
    blue_lambda_plot = plt.plot(range(150), np.maximum(blue_freq, 10**-1), "bo", alpha=0.5)
    #1blue_hist_plot = plt.plot(range(50), np.maximum(hist_blue, 10**-1), "ro")
    plt.xlabel("Donor Photons", labelpad=6)
    plt.ylabel("Frequency", labelpad=6)
    #plt.savefig(r"/home/rebecca/Documents/Klenermanproject/Figures/data_blue_7.pdf")
    plt.savefig("%s/%s" %(output_filepath, blue_graph))
    plt.savefig("%s/marginal_blue_6_few.svg" %output_filepath)
    #plt.show()
    plt.cla()

    

if __name__=="__main__":
    cfg_name = sys.argv[1]
    main(cfg_name)
