# importing the libraries needed to run the program
import numpy as np
from scipy import stats
import math as ma
import random
import matplotlib.pyplot as plt
import cPickle 
import os.path
import ConfigParser

# code to simulate data for a singly-labelled a-synuclein/aB/Tau molecule in solution.
# Because there is only one colour, I am going to assume that unlabelled monomers are indistinuishable from noise.

# Forward model of data to test fitting
def model_fwd(lam_prot, lam_NB, lam_DB, R_blue, count):
    # initialising list of blue photons
    data_blue = []
    
    for i in range(count):
        # initialising the photon count for each channel
        blue = 0.0 
         
        # determining how many protein molecules are present
        state = stats.poisson.rvs(lam_prot)
        
        # regardless of protein state, there will be a contribution from the noise distribution
        # add poisson-noise to time-bin i
        blue += stats.poisson.rvs(lam_NB)
        
        # add photons for each molecule in the confocal volume
        
        for j in range(state):
            # draw a local lambda from a gamma distribution with mean lam_DB -- this models diffusion through different    regions of the confocal volume
            lam_blue = random.gammavariate(R_blue, lam_DB/R_blue)
            # the blue dye emits photons from a poisson distribution with a mean that is the local dye mean lam_blue
            blue += stats.poisson.rvs(lam_blue)

        data_blue.append(int(blue))
    return data_blue

if __name__=="__main__":
    test = False
    basedir = "/media/TOSHIBA EXT/repos/fret-inference/test_data/monomers"
    filename = "blue_channel.txt"
    data_blue = model_fwd(0.06, 0.5, 8, 1.0, 100000)
    plt.hist(data_blue, bins=range(0, 40, 1), color="g")
    plt.show()
    plt.savefig("/home/rebecca/Documents/monomer_histogram.png") 
    f_blue = open("%s/%s" %(basedir, filename), "w")
    cPickle.dump(data_blue, f_blue)
    f_blue.close()
    
    ## Now write an inference config file
    config = ConfigParser.RawConfigParser()
    config.add_section('input')
    config.set('input', 'filepath', basedir)
    config.set('input', 'donor_name', filename)
    config.set('input', 'file_number', 1)

    blue_graph = "data_marginalB.pdf" 
    samples =  "data_samples" 
    outfolder = basedir           

    config.add_section('output')
    config.set('output', 'blue_graph', blue_graph)
    config.set('output', 'sample_file', samples)
    config.set('output', 'output_filepath', outfolder)
    
    if test == False:
        
        settings = [("burn_in_1", 3000), ("iterations_1", 1000),
("samples_1", 2), \

                    ("burn_in_2", 1000),
("iterations_2",100),
("samples_2", 100)]

    config.add_section('iterator parameters')
    for k,v in settings:
        config.set('iterator parameters', k, v)

    infer_file = os.path.join(basedir, "data_infer.cfg")
    with open(infer_file, 'wb') as configfile:
        config.write(configfile)


    

