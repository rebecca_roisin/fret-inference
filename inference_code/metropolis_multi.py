import math as ma
import numpy as np
import copy
import random
from scipy import stats

# Function that chooses a new value for a parameter by sampling from a normal distribution with a mean equal to the current value of the parameter    
def sample_maker_normal(sample_current, std = 1):
    sample_proposed = stats.norm.rvs(sample_current, std)
    return sample_proposed 
    
# Function that orders (sorts) the items in a list from smallest to largest, then returns the items that fall within the 98% confidence interval of the list.
def list_sorter(list1):
    list1 = sorted(list1)
    list_98 = np.mean(list1), list1[len(list1)*1/100], list1[len(list1)*99/100]
    return list_98 

# Function that selects between the current and proposed values for a set of parameters, based on the probability that the dataset was generated from these parameters.
def parameter_selection(probability_of_current, probability_of_proposed, alphaxxxx):
    changes = False
    a1 = probability_of_proposed - probability_of_current + ma.log(alphaxxxx)
    a = min(a1, 0.0)
    p = random.random()
    if ma.log(p) < a:
        changes = True
    return changes

# Function that evaluates the logistic function of an input value
def logistic(value):
    exp = ma.e**(-value)
    x = 1.0 / (1.0 + exp)
    return x

# Function that evaluates log(1 +e^x) for an input value
def logone(value):
    trigger = False
    exp = ma.e**value
    x = ma.log(1 + exp)
    if x == 0.0:
        x = 1e-300
        #print "Oops! Error in logone!"
        trigger = True
    return x, trigger
    
# Inverse of the logone function   
def inv_logone(value):  
    x = ma.log(ma.e**(value) - 1)
    if x == 0.0:
        x = 1e-300
        #print "Oops! Error in inv_logone!"
    return x

# differential of the inverse logone function
def inv_dif_logone(x):
    #num = ma.e**x
    #denom = ma.e**x - 1
    #frac = num / denom
    y = 1.0 - (ma.e**(-x))
    if y == 0.0:
        y = 1e-300
        #print "Oops! Error in inv_dif_logone!"
    frac = 1.0 / y
    return frac


# Logit log(x / (1-x))
def logit(x):
    y = ma.log(x / (1.0 - x))
    return y

# Function that gives a scaled logistic function
def clamp(v, min_x, max_x):
    trigger = False
    x = logistic(v)
    range_x = max_x - min_x
    x = (x*range_x) + min_x
    return x, trigger

# Inverse of the clamp function
def inv_clamp(x, min_x, max_x):
    range_x = max_x - min_x
    x = logit((x - min_x) / range_x)
    return x

# differential of the inverse clamp function
def inv_dif_clamp(x, min_x, max_x):
    p1 = 1.0 / (x - min_x)
    p2 = 1.0 / (max_x) - x
    val = p1 + p2
    return val

    
no_transform = ((lambda x: x), (lambda x: x), (lambda x: 1.0))

logone_transform = (logone, inv_logone, inv_dif_logone)

def clamp_transform(min_x, max_x):
    transform = lambda x: clamp(x, min_x, max_x)
    inverse = lambda x: inv_clamp(x, min_x, max_x)
    diff = lambda x: inv_dif_clamp(x, min_x, max_x)
    return (transform, inverse, diff)

## A set of classes to help you define samplers

class SamplerMeta(type):
    def __init__(cls, name, bases, dct):
        super(SamplerMeta, cls).__init__(name, bases, dct)

    def __new__(meta, name, bases, dct):
        # Save all sampling variables
        variables = []
        for var in dct:
            try:
                dct[var].set_name(var)
                variables += [dct[var]]
                dct[var] = dct[var].initial
            except:
                pass
                
        dct["all_variables"] = variables
        return super(SamplerMeta, meta).__new__(meta, name, bases, dct)

class Sampler(object):
    __metaclass__ = SamplerMeta

    def __init__(self, other = None):
        if other is None:
            return
        
        for v in self.all_variables:
            self.__dict__[v.name] = getattr(other, v.name)
            print "Set: %s = %s" % (v.name, self.__dict__[v.name])

    def initialise(self):
        target = {}
        all_v = {}

        for v in self.all_variables:
            initial = getattr(self, v.name)
            Var = v.get_var(initial, target)
            all_v[v.name] = Var

        sampling_variables = [all_v[v.name] for v in self.sampling_variables]

        return sampling_variables, target

    def Var_to_params(self, target, choice):
        for v in target:
            self.__dict__[v] = target[v][choice]

        return copy.copy(self)

class LogOneVar:
    def __init__(self, initial, step):
        self.initial = initial
        self.step = step

    def set_name(self, name):
        self.name = name

    def get_var(self, initial, target):
        return Variable(initial, self.step, logone_transform, self.name, target)

class ClampVar:
    def __init__(self, initial, step, lower, upper):
        self.initial = initial
        self.step = step
        self.lower = lower
        self.upper = upper

    def set_name(self, name):
        self.name = name

    def get_var(self, initial, target):
        return Variable(initial, self.step, clamp_transform(self.lower, self.upper), self.name, target)



# To write a variable that needs to be clamped: 
# p_label_red = variable(0.95, 0.2, clamp_transform(0.5, 1.0)) 

# class that contains all the variables sampled in the iteration process
class Variable:
   # __slots__ = ("name", "transform","inverse", "v_current", "v_proposed", "std", "samples")

    def __init__(self, initial, std_initial, transform, name, target = None):
        self.name = name
        self.transform, self.inverse, self.diff = transform  
        v_initial = self.inverse(initial)
        self.v_current = v_initial
        self.v_proposed = v_initial
        #self.hold1 = v_initial
        #self.hold2 = v_initial
        self.samples = []
        self.std = std_initial
        self.std_big = 10
        if target != None:
            self.target = target
            # self.index = len(target)
            #target.append(self.value())
            self.target[self.name] = self.value()
        else:
            self.target = None
            self.index = None
    
    # This is one problem
    def refresh_target(self):
        if self.target != None:
            self.target[self.name] = self.value()
    
    def value(self):
        # modified to ignore trigger
        x1, _ = self.transform(self.v_current)
        x2, _ = self.transform(self.v_proposed)
        return (x1, x2) # returns a tuple

    # This is another problem
    def get_alpha(self):
        # modified to return trigger value
        dxp, trigp = self.transform(self.v_proposed) 
        dxc, trigc  = self.transform(self.v_current)
        a = self.diff(dxc) / self.diff(dxp)
        return a, trigp, trigc

    def update(self):
        self.v_current = self.v_proposed
        self.refresh_target()

    def proposal(self):
        self.v_proposed = sample_maker_normal(self.v_current, self.std)
        self.refresh_target()

    def radical_proposal(self):
        self.v_proposed = sample_maker_normal(self.v_current, self.std_big)
        self.refresh_target()


    def abort(self):
        self.v_proposed = self.v_current
        self.refresh_target()

    def save_sample(self):
        current, _ = self.value()
        self.samples.append(current)

    def print_sample(self):
        list_mean, list_1, list_99 = list_sorter(self.samples)
        print "%s Interval 98 = %.3f, [%.3f, %.3f]" %(self.name, list_mean, list_1, list_99)
        return list_mean, list_1, list_99 
    
    def sample_mean(self):
        mean = np.mean(self.samples)
        return mean

def iterator(data, sampling_variables, target, switch_names, coswitch_names, params, probability_evaluator, sample_storer, sample_number = 5, burn_in = 4000, sample_iterations = 1000):
    # target = {}
    stored_sample_params = []
    stored_probabilities = []
    stored_prob_list = []
    counter = 0
    changes = 0
    sample_counter = 0
    prints = False
    trigger = False
        
    current_probability = None
    
    while True:
        selected_variable = random.choice(sampling_variables)
        if selected_variable.name == "lambda dye blue":
            prints = True
        selected_variable.proposal() 
        alphaxxx, trigp, trigc = selected_variable.get_alpha()
        if trigp or trigc: # this tells us that we got a zero value in logone
            trigger = True
            # we want to know what happened
            print "Woah! Something went wrong here!"
                                     
        if current_probability == None:
            current_probability, prob_list = probability_evaluator(data, params, target, 0)

        if selected_variable.name == "lambda dye blue":
            prints = True
        
        proposed_probability, prob_list_prop = probability_evaluator(data, params, target, 1)
       
        try:
            change = parameter_selection(current_probability, proposed_probability, alphaxxx) 
        except:
            print selected_variable.name
            print alphaxxx
            raise
                 
        if change:
            selected_variable.update()
            current_probability = proposed_probability
            prob_list = prob_list_prop
            changes += 1

                            
            #if selected_variable.name == "lambda dye blue":
            #    print "lam_DB updated", selected_variable.value()[0]      

        else:
            selected_variable.abort()             
                
        counter += 1
        
        
        if counter %sample_iterations == 0 and counter >= burn_in:
            sample_counter += 1
            for v in sampling_variables:
                v.save_sample()
                v.print_sample()
            stored_sample_params.append(sample_storer(params, target))
            stored_probabilities.append(current_probability)
            stored_prob_list.append(prob_list)
            total_samples = sample_counter               
            change_ratio = float(changes) / counter
           
           
            print "change ratio = %3f" %(change_ratio)
            print "current probability = %3f" %(current_probability)
            print "\n"

        if sample_counter >= sample_number:
            #f = file("/home/rebecca/Documents/Klenermanproject/infer/info_synthetic_multi_donor.txt", "w")
            for var in sampling_variables:
                #print len(var.samples)
                list_mean_var, list_1_var, list_99_var = list_sorter(var.samples)
                #f.write("%s Interval 98 = %.3f, [%.3f, %.3f] \n" %(var.name, list_mean_var, list_1_var, list_99_var))            
            #f.close()
            return map(Variable.sample_mean, sampling_variables), stored_sample_params, stored_probabilities, stored_prob_list, trigger
