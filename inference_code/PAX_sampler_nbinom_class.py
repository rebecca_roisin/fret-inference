from scipy import stats
import numpy as np
np.seterr(invalid='raise')

import math as ma
import random
import numpy.random as npr
import cPickle
import array
from scipy.special import gamma, gammaln
import MCS_reader
import matplotlib.pyplot as plt
import profile
from metropolis_multi import *
from FRET_standard import *
import sys
import os.path
import copy
import ConfigParser

from integrate_debug import *

# Making a Metropolis-Hastings Monte-Carlo sampler that will estimate the parameters of Poisson distributions that fit 2-colour FRET data.  Initially, we will assume a single population.  This will be extended to multiple FRET populations once the simple sampler runs.

#Collecting (synthetic) data from a file
def file_reader(file1):
    f = open(file1, "r")
    data = cPickle.load(f)
    data = np.array(data)
    print data
    f.close()
    return data

# Function that finds the maximum of the posterior distribution of lambdas.
def quad_solve(E, lam_NB, blue, r, lam_DB):
    x = 1.0 - E
    a = -((x**2) + (r*x/lam_DB)) 
    b = (blue*x) + (x*r) - (x + (x*lam_NB) + (r*lam_NB/lam_DB))
    c = (lam_NB*r) - lam_NB

    g = (b**2 - (4*a*c))**0.5
    if a == 0:
        x1 = (-b + g)
        x2 = (-b - g) 
        print "Oops!"
    else:
        x1 = (-b + g) / (2*a)
        x2 = (-b - g) / (2*a)
    return x2
    
# Function that takes two data inputs and creates a dictionary of the frequencies at which different pairs of values (from the two inputs) occur.
def data_counter(data_blue, data_red):
    # print data_blue, data_red
    blue_array = []
    red_array = []
    counter = []
    counted_data = {}
    for i, j in zip(data_blue, data_red):
        if (i,j) not in counted_data:
            counted_data[(i,j)] = 0
        counted_data[(i,j)] += 1
    for (i,j), k in counted_data.iteritems():
        blue_array.append(i)
        red_array.append(j)
        counter.append(k)
    blue_array = np.array(blue_array)
    red_array = np.array(red_array)
    counter = np.array(counter)
    print "length of input = %s \nlength of output = %s \n" %(len(data_blue), len(blue_array))
    return blue_array, red_array, counter
   
# preparing the cache for the poisson_broad function
seq_ref = np.array(range(300))
gamma_ref = gamma(seq_ref + 1)
dist_cache = {}
cache_len = 0

# Function that samples from a Poisson distribution    
def poisson_pmf(data, lam):
    global dist_cache, cache_len
    if lam in dist_cache:
        # Cache maintenance
        ppmf = dist_cache[lam]        
    else:
        lamLN = ma.log(lam)
        ppmf = ma.e**(seq_ref*lamLN - gammaln(seq_ref + 1) - lam)
        
        if cache_len > 1000:
            dist_cache = {}
            cache_len = 0
        #binom[binom < 0.00000000000001] = 0.0
        dist_cache[lam] = ppmf
        cache_len += 1
    
    return ppmf[data]

# Function that samples from an overdispersed poisson distribution
def poisson_broad(data, lam, r = 4):
    global dist_cache, cache_len
    if (r, lam) in dist_cache:
        # Cache maintenance
        binom = dist_cache[(r, lam)]        
    else:
        p = lam / (r + lam)
        coeff = ma.e**(gammaln(seq_ref + r) - ((gammaln(seq_ref + 1) + gammaln(r))))
        binom = coeff * ((1-p)**r) * (p**seq_ref)
        if cache_len > 1000:
            dist_cache = {}
            cache_len = 0
        #binom[binom < 0.00000000000001] = 0.0
        dist_cache[(r, lam)] = binom
        cache_len += 1
    
    return binom[data]

def poisson_broad_slow(data, lam, r = 4):

    p = lam / (r + lam)
    #coeff = gamma(data + r) / ((gamma(data + 1) * gamma(r)))
    coeff = ma.e**(gammaln(data+r) - (gammaln(data+1) + gammaln(r)))
    binom = coeff * ((1-p)**r) * (p**data)
    return binom


#np.seterr(over = "raise")

def poisson_broad_sum2(data, lam1, R1, lam2, R2):
    global dist_cache, cache_len
    if (R1, lam1, R2, lam2) in dist_cache:
        ps = dist_cache[(R1, lam1, R2, lam2)]
    else:
        p1 = poisson_broad(seq_ref, lam1, R1)
        p2 = poisson_broad(seq_ref, lam2, R2)
        ps = np.convolve(p1, p2, "full")

        if cache_len > 1000:
            dist_cache = {}
            cache_len = 0
        dist_cache[(R1, lam1, R2, lam2)] = ps
        cache_len += 1
    return ps[data]

def distribution(data_blue, data_red, counter, value):
    red_x_array = np.zeros(300)
    # blue, red, counter = data
    for b, r, count in zip(data_blue, data_red, counter):
        if b == value:
            red_x_array[r] += count
    print red_x_array
    return red_x_array                                
                        
# Core probability evaluator
def model(data_blue, data_red, counter, params, ApproxModel = True):

    lam_prot, lam_NB, lam_NR, p_labelB, p_labelR, lamDB, r_sep, R_blue, gamma_ins, R0, R_approx = params.lam_prot, params.lam_NB, params.lam_NR, params.pLabelB, params.pLabelR, params.lamDB, params.r_sep, params.R_blue, params.gamma_ins, params.R0, params.R_approx 

    # wide priors for the data
    prior_DB = scipy.stats.norm.pdf(lamDB, 20, 5) 
    prior_prot = scipy.stats.expon.pdf(lam_prot, mean=0.01)
    prior = prior_DB * prior_prot
    
    p_prot0, p_prot1, p_prot2 = poisson_pmf(0, lam_prot), poisson_pmf(1, lam_prot), poisson_pmf(2, lam_prot)    
    p_prot_many = 1.0 - p_prot0 - p_prot1
    
    # Blue channel probabilities 
    E = 1.0 / (1 + ((float(r_sep) / R0)**6))

    # New probabilities using different labeling probabilities
    x = p_labelB    
    y = p_labelR    

    p_none = (1-x)*(1-y)
    p_blue = x*(1-y)
    p_red = (1-x)*y
    p_both = x*y
    assert 0.9999 < p_none + p_blue + p_red + p_both < 1.0001
    r_noise = 50  

    # probability from noise only
    P_noise = p_prot0 + p_prot1 * (p_none + p_red) + p_prot2*(p_none*p_none + 2*p_none*p_red + p_red*p_red)
    pB_noise = P_noise * poisson_pmf(data_blue, lam_NB)
    pR_noise = pB_noise * poisson_pmf(data_red, lam_NR)
    
    # probability from one blue dye    
    P_dye_1 = p_prot1*p_blue  + p_prot2*(2*p_none*p_blue + 2*p_red*p_blue)
    pB_dye_1 = P_dye_1 * poisson_broad_sum2(data_blue, lam_NB, r_noise, lamDB, R_blue)    
    pR_dye_1 = pB_dye_1 * poisson_pmf(data_red, lam_NR)
    
    # probability from single FRET event
    P_FRET_1 = p_prot1 * p_both + p_prot2*(2*p_none*p_both + 2*p_both*p_red)

    if ApproxModel:
        ## Approximation using best lambda
        pB_FRET_1 = P_FRET_1 * poisson_broad_sum2(data_blue, lam_NB, r_noise, (lamDB*(1.0-E)), R_blue)    
        blue_prime = quad_solve(E, lam_NB, data_blue, R_blue, lamDB)
        pR_FRET_1 = pB_FRET_1 * poisson_broad_slow(data_red, lam_NR + (gamma_ins*blue_prime*E), R_approx)
    else:
        # for accurate integration -- split into two parts      
        s = cachedmanymc(data_blue, data_red, lam_NB, lam_NR, R_blue, E, gamma_ins, lamDB, 0, samples = 5000, start = 0, LIMIT = 10.0)
        t = cachedmanymc(data_blue, data_red, lam_NB, lam_NR, R_blue, E, gamma_ins, lamDB, 0, samples = 100, start = 10.0, LIMIT = 100.0)
        pR_FRET_1 = (P_FRET_1 * s) + (P_FRET_1 * t)

    # The case of multiple (i.e. 2) dyes
    p_multiple = p_prot_many - p_prot2*(p_none*p_none + 2*p_none*p_red + p_red*p_red + 2*p_none*p_blue + 2*p_none*p_both + 2*p_red*p_blue + 2*p_both*p_red)    
    pB_many = p_multiple *  poisson_broad(data_blue, lam_NB + (2*lamDB*(1.0-E)), 1.0)
    pR_many = pB_many * poisson_broad_slow(data_red, lam_NR + data_blue * ( E * gamma_ins) / (1.0-E + 1e-27), 1.0)    

    ## Sanity Check
    try:
        assert(0.99 < P_noise + P_dye_1 + P_FRET_1 + p_multiple <= 1.01)
    except:
        raise

    prob_list = (P_noise, P_dye_1, P_FRET_1, p_multiple, prior_DB, prior_prot, prior)
    p_tot_R = pR_noise + pR_dye_1 + pR_FRET_1 + pR_many
    
    p_tot = np.log(p_tot_R)
    p_tots = np.sum(p_tot * counter) + np.log(prior)

    try:
        assert(p_tots <= 0.0)
    except:
        #if p_tots > 0.0:
        print "Probability greater than One!"
        print "Total probability = %s, protein_1 = %s, many = %s, noise = %s" %(p_tots, np.sum(np.log(p_tot_R*counter)), np.sum(np.log(pR_many*counter)), np.sum(np.log(pR_noise*counter)))
        print "Parameters: lam_NB = %s, lam_NR = %s, R_blue = %s, E = %s, gamma_ins = %s, lamDB = %s" %(lam_NB, lam_NR, R_blue, E, gamma_ins, lamDB)
        #print "Total: %s" % p_tot_R
        #print "noise: %s" % sum(pR_noise)
        #print "dye_1: %s" % sum(pR_dye_1)
        print "FRET_1: %s" % sum(pR_FRET_1)
        for b,r,x in zip(data_blue, data_red, pR_FRET_1):
            print "(%s,%s) %s" % (b,r,x)
        fb = open("/media/TOSHIBA EXT/repos/fret-inference/test_data/cfg_data/30_synthetic_p2_debug1/results/messup.csv", "w")
        for i in s:        
            fb.write("%s\n" %i)
        #for m in gg:
        #    fb.write("%s," %m)
        fb.close()
        #print gg, p0s, p1s     
        raise
    
    if ma.isnan(p_tots) or ma.isinf(p_tots):
        print "noise: %s" % sum(pR_noise)
        print "dye_1: %s" % sum(pR_dye_1)
        print "FRET_1: %s" % sum(pR_FRET_1)
        raise Exception("Nan or Inf: %s" % p_tots)

    # Return detailed counts, or not, depending on whether you are evaluating for histogram
    #print "p_tots", p_tots
    return p_tots, p_tot_R, None, prob_list # prFRET
    

# Function that evaluates the probability that a dataset was generated from a given set of parameters (pProt, lam_NB, lam_NR, pBlue, pRed, lam_DB, lam_DR, r).  Evalulates individually each datapoint in the dataset.  Data should be in array form.  Complete but not tested.  
def probability_evaluator_fast(data, params, target, choice):

    data_blue, data_red, counter = data
    params = params.Var_to_params(target, choice)

    logprob, _, _, prob_list = model(data_blue, data_red, counter, params, ApproxModel = True)
    return logprob, prob_list

## The evaluator that uses the slow function
def probability_evaluator(data, params, target, choice):

    data_blue, data_red, counter = data
    params = params.Var_to_params(target, choice)

    logprob, _, _, prob_list = model(data_blue, data_red, counter, params, ApproxModel = False)
    return logprob, prob_list

class FRETmodel(Sampler):
    R0 = 56.0

    gamma_ins = LogOneVar(1.0, step = 0.1)
    lam_prot = LogOneVar(0.05, step = 0.2)
    lam_NB = LogOneVar(1.0, step = 0.5)
    lam_NR = LogOneVar(1.0, step = 0.5)
    pLabelB = ClampVar(0.4, 0.2, 0.2, 1)
    pLabelR = ClampVar(0.8, 0.2, 0.2, 1)  
    lamDB = LogOneVar(8.0, step = 0.5)  

    r_sep = LogOneVar(40, step = 1.0)  
    R_blue = LogOneVar(1.0, step = 0.5)
    R_approx = LogOneVar(30.0, step = 0.5)

    sampling_variables = [lam_prot, lam_NB, lam_NR, lamDB, r_sep]

class FRETmodelPhase2(Sampler):
    R0 = 56.0

    gamma_ins = LogOneVar(1.0, step = 0.1)
    lam_prot = LogOneVar(0.05, step = 0.2)
    lam_NB = LogOneVar(1.0, step = 0.5)
    lam_NR = LogOneVar(1.0, step = 0.5)
    pLabelB = ClampVar(0.4, 0.2, 0.2, 1)
    pLabelR = ClampVar(0.8, 0.2, 0.2, 1)  
    lamDB = LogOneVar(8.0, step = 0.5)  

    r_sep = LogOneVar(40, step = 1.0)  
    R_blue = LogOneVar(1.0, step = 0.5)
    R_approx = LogOneVar(30.0, step = 0.5)

    sampling_variables = [lam_prot, lamDB, r_sep]    

def lambda_histogram(params, total_points, buckets = 50):

    data_blue = []
    data_red = []
    counter = []
    
    for b in range(buckets):
        for r in range(buckets):
            data_blue += [b]
            data_red += [r]            
            counter += [1]

    data_blue = np.array(data_blue)
    data_red = np.array(data_red)    
    counter  = np.array(counter)

    # model(data_blue, data_red, counter, lam_prot, lam_NB, lam_NR, p_labelB, p_labelR, lam_DB, r, R_blue, gamma_ins, R0)

    _, probs, _, _ = model(data_blue, data_red, counter, params)

    print "Sum of probs == 1? %s" % (sum(probs))

    freq = probs * total_points
                
    blue_freq = np.zeros(buckets)
    red_freq = np.zeros(buckets)

    for (b, r, v) in zip(data_blue, data_red, freq):
         blue_freq[b] += v
         red_freq[r] += v

    return blue_freq, red_freq

def main_iterators(blue_data, red_data, counter, params, config):
    sample_storer = lambda p, t: p.Var_to_params(t, True)
    
    # parameters for approximate inference    
    burn_in_1 = config.getint('iterator parameters', 'burn_in_1')
    iterations_1 = config.getint('iterator parameters', 'iterations_1')
    samples_1 = config.getint('iterator parameters', 'samples_1')

    # parameters for exact inference
    burn_in_2 = config.getint('iterator parameters', 'burn_in_2')
    iterations_2 = config.getint('iterator parameters', 'iterations_2')
    samples_2 = config.getint('iterator parameters', 'samples_2')

    print "Sampling params"
    print burn_in_1, iterations_1, samples_1
    print burn_in_2, iterations_2, samples_2
    print 

    # doing the approximate inference
    my_params = FRETmodel(params)
    sampling_variable, target = my_params.initialise() # ApproxModel = True)
    results, samples, stored_probabilities, prob_list, trigger = iterator((blue_data, red_data, counter), sampling_variable, target, None, None, my_params, probability_evaluator_fast, sample_storer, sample_number = samples_1, burn_in = burn_in_1, sample_iterations = iterations_1)
    (my_params.lam_prot, my_params.lam_NB, my_params.lam_NR, my_params.lamDB, my_params.r_sep) = results

    print "\n\nPHASE 2 -- Precise estimation\n\n"    

    my_params_p2 = FRETmodelPhase2(my_params)
    ## Second phase -- nail the exact values for r_sep
    sampling_variable, target = my_params_p2.initialise() # ApproxModel = False)

    results, samples, stored_probabilities, prob_list, trigger = iterator((blue_data, red_data, counter), sampling_variable, target, None, None, my_params_p2, probability_evaluator, sample_storer, sample_number = samples_2, burn_in = burn_in_2, sample_iterations = iterations_2)
    (my_params_p2.lam_prot, my_params_p2.lamDB, my_params_p2.r_sep) = results

      
    return my_params_p2, samples, stored_probabilities, prob_list 

def main(configname, synthetic = True):
    #bp = sys.argv[1]
    #print "Analysis for %s" % bp
    if synthetic == True:
        try:
            config = ConfigParser.RawConfigParser({})
            config.read(configname)

            filepath, blue_file, red_file, no_files = config.get('input', 'filepath'), config.get('input', 'donor_name'), config.get('input', 'acceptor_name'), config.getint('input', 'file_number')
            #file_start, file_end = config.getint('input', 'file_start'), config.getint('input', 'file_end')
            #blue_file, red_file, no_files = sys.argv[1], sys.argv[2], int(sys.argv[3])
            #print "File names read" 
        except:
            print "Usage: python PAX_sampler_nbinom_class_multi.py blue_pattern red_pattern number"
            print "/media/KINGSTON/20121026/3/DNA_d_ /media/KINGSTON/20121026/3/DNA_a_ 60"
            raise
        #print "Analysis for %s" % bp
        print "Analysis of DNA mixture data"

        blue_channel = file_reader("%s/%s" %(filepath, blue_file))
        red_channel = file_reader("%s/%s" %(filepath, red_file))
        print "Data files read"
        #print "Here is the raw blue data:", blue_channel

    elif synthetic == False:
        config = ConfigParser.RawConfigParser({})
        config.read(configname)

        filepath, blue_file, red_file, no_files = config.get('input', 'filepath'), config.get('input', 'donor_name'), config.get('input', 'acceptor_name'), config.getint('input', 'file_number')
        file_start, file_end = config.getint('input', 'file_start'), config.getint('input', 'file_end')

        data_blue1 = []
        data_red1 = []

        try:
            for i in range(file_start, file_end):
                data_blue1 += MCS_reader.MCS_process("%s/%s%04d.mcs" % (filepath, blue_file, i) )
                data_red1 += MCS_reader.MCS_process("%s/%s%04d.mcs" % (filepath, red_file, i) )
        except:
            print "Files may not exist"# , i        
            raise    

        ## Filter large values
        data_blue = [db for (db,dr) in zip(data_blue1, data_red1) if db < 300 and dr < 300] 
        data_red = [dr for (db,dr) in zip(data_blue1, data_red1) if db < 300 and dr < 300]
     
        blue_channel = np.array(data_blue)
        red_channel = np.array(data_red)


    blue_data, red_data, counter = data_counter(blue_channel, red_channel)

    my_params = FRETmodel()
    my_params.pLabelB = config.getfloat('variable parameters', 'p_labelB')
    my_params.pLabelR = config.getfloat('variable parameters', 'p_labelR')
    print "p_label: blue %s red %s" % (my_params.pLabelB, my_params.pLabelR)

    print "\n\nPHASE 1 -- Approximate estimation\n\n"
    my_params, samples, stored_probabilities, prob_list = main_iterators(blue_data, red_data, counter, my_params, config)
    print "Storing Samples"    
    output_filepath = config.get('output', 'output_filepath')
    storage_name = config.get('output', 'sample_file')

    f_pickle = open("%s/%s.pkl" %(output_filepath, storage_name), "w")    
    cPickle.dump(samples, f_pickle)
    f_pickle.close()

    # csv file
    f_csv = open("%s/%s.csv" %(output_filepath, storage_name), "w")
    f_csv.write("lam_prot, lamDB, r_sep, probability, P_noise, P_dye_1, P_FRET_1, p_multiple, prior_DB, prior_prot, prior\n")
    for p, q, r in zip(samples, stored_probabilities, prob_list):
        f_csv.write("%s, %s, %s, %s" %(p.lam_prot, p.lamDB, p.r_sep, q))
        for el in r:
            f_csv.write(", %s" %el)
        f_csv.write("\n")        
    f_csv.close()
        

    print "\n\nPHASE 3 -- Plotting histograms\n\n"

    blue_freq, red_freq = lambda_histogram(my_params, sum(counter), buckets = 150)

    # file names for graphs
    blue_graph = config.get('output', 'blue_graph')
    red_graph = config.get('output', 'red_graph')

    blue_histogram = plt.hist(blue_channel, bins = range(150), log = True, facecolor='none')
    # plt.ylim(ymax=000)
    blue_lambda_plot = plt.plot(range(150), np.maximum(blue_freq, 10**-1), "bo", alpha=0.5)
    #1blue_hist_plot = plt.plot(range(50), np.maximum(hist_blue, 10**-1), "ro")
    plt.xlabel("Donor Photons", labelpad=6)
    plt.ylabel("Frequency", labelpad=6)
    #plt.savefig(r"/home/rebecca/Documents/Klenermanproject/Figures/data_blue_7.pdf")
    plt.savefig("%s/%s" %(output_filepath, blue_graph))
    plt.savefig("%s/marginal_blue_6_few.svg" %output_filepath)
    #plt.show()
    plt.cla()

    red_histogram = plt.hist(red_channel, bins = range(150), log = True, facecolor='none')
    # plt.ylim(ymax=1000)
    red_lambda_plot = plt.plot(range(150), np.maximum(red_freq, 10**-1), "ro", alpha=0.5)
    #1red_hist_plot = plt.plot(range(50), np.maximum(hist_red, 10**-1), "ro")
    plt.xlabel("Acceptor Photons", labelpad=6)
    plt.ylabel("Frequency", labelpad=6)
    #1plt.savefig(r"/home/rebecca/Documents/Klenermanproject/Figures/data_red_7.pdf")
    plt.savefig("%s/%s" %(output_filepath, red_graph))
    plt.savefig("%s/marginal_red_6_few.svg" %output_filepath)
    #plt.show()
    #1plt.cla()

if __name__=="__main__":
    cfg_name = sys.argv[1]
    main(cfg_name)
