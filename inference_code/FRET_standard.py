import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
import MCS_reader 

# open data files and make arrays
def data_process(datafile, datalist):
    #data = []    
    f = open(datafile, "r")
    datalist += MCS_reader.MCS_process(datafile)
    #data = np.array(data)
    f.close()
    return datalist

def file_reader(file1):
    f = open(file1, "r")
    data = cPickle.load(f)
    data = np.array(data)
    print data
    f.close()
    return data

# standard manner of evaluating FRET data - for comparison: AND threshold
def FRET_standard(blue, red, bthr, rthr):
    data_blue_threshold = []
    data_red_threshold = []
    for db, dr in zip(blue, red):
        if db > bthr:
            if dr > rthr:
                data_blue_threshold.append(db)
                data_red_threshold.append(dr)
    print data_blue_threshold, data_red_threshold
    E_list = []
    for i, j in zip(data_blue_threshold, data_red_threshold):
        #E = float(j) / (float(i) + float(j))
        E = float(j) / (float(i) + float(j))
        #print i, j, E
        E_list.append(E)
    E_list = np.array(E_list)
    print E_list
    return E_list

                
    #data_blue_threshold = [db for (db,dr) in zip(blue, red) if bthr < db and rthr < dr]
    #data_red_threshold = [dr for (db,dr) in zip(blue, red) if bthr < db and rthr < dr]
    
    #E_list = []
    #for i, j in zip(data_blue_threshold, data_red_threshold):
    #    E = float(j) / (float(i) + float(j))
    #    print i, j, E
    #    E_list.append(float(j) / (float(i) + float(j)))
    #    #E_list.append(E)
    #E_list = np.array(E_list)
    #print E_list
    #E_mean = np.mean(E_list)
    #return E_list

# sum thresholding
def FRET_sum_standard(blue, red, threshold):
    data_blue_threshold = []
    data_red_threshold = []
    for db, dr in zip(blue, red):
        if db + dr > threshold:
            data_blue_threshold.append(db)
            data_red_threshold.append(dr)
    #data_blue_threshold = [db for (db,dr) in zip(blue, red) if db + dr > threshold] #4 < db < 125 and 4 < dr < 125]
    #data_red_threshold = [dr for (db,dr) in zip(blue, red) if db + dr > threshold] #4 < db < 125 and 4 < dr < 125]
    E_list = []
    for i, j in zip(data_blue_threshold, data_red_threshold):
        E = float(j) / (float(i) + float(j))
        E_list.append(E)
    E_list = np.array(E_list)
    E_mean = np.mean(E_list)
    E_std = np.std(E_list)
    r_mean = 56*((1 - E_mean)/E_mean)**(1.0/6)
    return E_list

    

# what the heck was this for??
def print_table(blue_data, red_data, counter):
    count_dict = dict([((b, r), c) for b,r,c in zip(blue_data, red_data, counter)])
    for b in range(20):
        s = "%2d: " % b
        for r in range (20):
            s += "%5d " % count_dict.get((b,r), 0)
        print s 
if __name__ == "__main__":
    # doing the analysis
    blue_data = []
    red_data = []
    #for k in range(120):
    #    data_blue = data_process("/home/rebecca/Documents/Klenermanproject/20121031/FRET2/DNA_d_%04d.mcs" %(k), blue_data)
    #    data_red = data_process("/home/rebecca/Documents/Klenermanproject/20121031/FRET2/DNA_a_%04d.mcs" %(k), red_data)

    data_blue = file_reader("/media/TOSHIBA EXT/synthetic_data/blue_synthetic_40and60_1_%s.dat")
    data_red = file_reader("/media/TOSHIBA EXT/synthetic_data/red_synthetic_40and60_1_%s.dat")

    data_blue = np.array(data_blue)
    data_red = np.array(data_red)

    print data_blue, data_red

    AND_E = FRET_standard(data_blue, data_red, 10, 10)
    SUM_E = FRET_sum_standard(data_blue, data_red, 20)

    # plotting the graphs
    bins = np.arange(0.0, 1.0, 0.015)
    plt.hist(AND_E, bins, facecolor = "grey")
    plt.xlabel("E")
    plt.ylabel("Number of Events")
    plt.savefig(r"/media/TOSHIBA EXT/inference_figures_new/8bp_DNA_AND_1010.png")
    plt.show()
    plt.cla()
    plt.hist(SUM_E, bins, facecolor = "grey")
    plt.xlabel("E")
    plt.ylabel("Number of Events")
    plt.savefig(r"/media/TOSHIBA EXT/inference_figures_new/8bp_DNA_SUM_20.png")
    plt.show()
    plt.cla()

